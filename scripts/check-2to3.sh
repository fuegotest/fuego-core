#!/bin/bash
#
# Use 2to3 to show the outstanding Python2 vs. Python3 compatibility
# issues with the 'ftc' program
#
# as of 2022-02-07, the following issue types are still reported
#
# they may or may not need remediation:
#
# 1) importlib is used to perform a special 'reload' operation on sys
# in order to cure a stupid bug with Python2 unicode handling
# I don't think this needs to be changed, but possibly 'reload'
# is not present in Python3
#
# 2) urllib has been refactored to put some routines in sub-modules
#
# 3) use of list(dict.items()) instead of just dict.items()
# I don't think this change is needed when dict.itesm() is used in a for loop.
#
# 4) raw_input vs input
# we need a wrapper that works for both python2 and python3 here
#
# 5) use of list(zip(...) instead of just zip(...)
#
# 6) use of list(dict.keys()) instead of just dict.keys()
# This is for test_spec_data, and I think this one needs to change.

2to3 -x print -x long -x input ftc
