# !/bin/bash
#
# Copyright (C) 2019 Toshiba corp.
# Author: Daniel Sangorrin <daniel.sangorrin@toshiba.co.jp>
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of version 2 of the GNU Lesser General Public License
# as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Bash completion script for Fuego's command line tool 'ftc'
#
# To enable the completions copy this file to
# /etc/bash_completion.d/ftc
# and make sure that your user's bashrc calls
# . /etc/bash_completion

in_array() {
    local i
    for i in "${@:2}"; do
        [[ $1 = "$i" ]] && return
    done
}

_ftc()
{
    local cur prev words cword
    local global_opts="-h --help -v -q -c -x --debug"
    declare -A percommand_opts=(
        ["add-jobs"]="-b -p -t -s --timeout --rebuild --reboot --precleanup --postcleanup"
        ["add-nodes"]="-f -b"
        ["add-view"]=""
        ["build-jobs"]=""
        ["config"]="-l"
        ["delete-var"]="-b"
        ["gen-report"]="--where --format --header_fields --fields --layout -o"
        ["help"]=""
        ["install-test"]="-u"
        ["list-boards"]="-q"
        ["list-jobs"]="-q"
        ["list-nodes"]="-q"
        ["list-plans"]="-q"
        ["list-requests"]="-q"
        ["list-runs"]="-q --where"
        ["list-specs"]="-q -t"
        ["list-tests"]="-q"
        ["package-run"]="-o -f"
        ["package-test"]="-o -f"
        ["power-on"]="-b"
        ["power-off"]="-b"
        ["power-cycle"]="-b"
        ["put-request"]="-R -s"
        ["put-run"]=""
        ["put-test"]=""
        ["query-board"]="-b -n --sh -q"
        ["release-resource"]="-b"
        ["reserve-resource"]="-f -b"
        ["rm-jobs"]="--remove-logs"
        ["rm-nodes"]=""
        ["rm-request"]=""
        ["run-request"]="--put-run"
        ["run-test"]="-b -t -s -p --timeout --rebuild --reboot --precleanup --postcleanup --dynamic-vars"
        ["set-var"]="-b"
        ["version"]=""
        ["wait-for"]="-i -t")

    _init_completion || return

    command="none"
    for word in "${COMP_WORDS[@]}"; do
        if in_array "$word" "${!percommand_opts[@]}"; then
            command=$word
            break
        fi
    done

    # if no command yet, autocomplete with global options or commands
    if [ "$command" == "none" ]; then
        COMPREPLY=( $( compgen -W "$global_opts ${!percommand_opts[*]}" -- "$cur" ) )
        return 0
    fi

    # otherwise, autocomplete with per-command options
    if [[ "$cur" == -* ]]; then
        COMPREPLY=( $( compgen -W "${percommand_opts[$command]}" -- "$cur" ) )
        return 0
    fi

    # if prev is an option, make sure it is allowed for the command
    if [[ "$prev" == -* ]]; then
        if [[ "${percommand_opts[$command]}" != *"$prev"* ]]; then
            return 0
        fi
    fi

    # try to autocomplete each option
    case $prev in
        '-b')
            # FIXTHIS: support multiple boards separated by commas
            COMPREPLY=( $( compgen -W "$(ftc list-boards -q)" -- "$cur" ) )
            return 0
            ;;
        '-t')
            #FIXTHIS: distinguish test from timeout
            COMPREPLY=( $( compgen -W "$(ftc list-tests -q ; ftc list-tests -q | sed s/^Functional.// | sed s/^Benchmark.// )" -- "$cur" ) )
            return 0
            ;;
        '-p')
            COMPREPLY=( $( compgen -W "$(ftc list-plans -q)" -- "$cur" ) )
            return 0
            ;;
        '--timeout')
            #FIXTHIS: try to complete
            return 0
            ;;
        '--rebuild' | '--reboot' | '--postcleanup' | '--precleanup')
            COMPREPLY=( $( compgen -W "true false" -- "$cur" ) )
            return 0
            ;;
        'put-run')
            COMPREPLY=( $( compgen -W "$(ftc list-runs -q)" -- "$cur" ) )
            return 0
            ;;
        'package-test')
            COMPREPLY=( $( compgen -W "$(ftc list-tests -q)" -- "$cur" ) )
            return 0
            ;;
        '--format')
            COMPREPLY=( $( compgen -W "txt html pdf excel csv rst" -- "$cur" ) )
            return 0
            ;;
        '-s')
            local i TEST=""
            for (( i=0; i < cword; i++ )); do
                if [[ "${words[i]}" == -t ]]; then
                TEST="${words[i + 1]}"
                fi
            done
            if [ -z "$TEST" ]; then
                return 0
            else
                COMPREPLY=( $( compgen -W "$(ftc list-specs -t $TEST -q)" -- "$cur" ) )
            fi
            return 0
            ;;
    esac

    # deal with commands that use no opts
    if [[ "$command" == "build-jobs" || "$command" == "rm-jobs" ]]; then
            COMPREPLY=( $( compgen -W "$(ftc list-jobs -q)" -- "$cur" ) )
            return 0
    fi

    if [[ "$command" == "rm-nodes" ]]; then
            COMPREPLY=( $( compgen -W "$(ftc list-nodes -q)" -- "$cur" ) )
            return 0
    fi

    if [[ "$command" == "rm-request" ]]; then
            COMPREPLY=( $( compgen -W "$(ftc list-requests -q)" -- "$cur" ) )
            return 0
    fi

    # if we have nothing, show available opts
    COMPREPLY=( $( compgen -W "${percommand_opts[$command]}" -- "$cur" ) )
} &&
complete -F _ftc ftc

# ex: ts=4 sw=4 et filetype=sh
