#!/bin/bash
#
# make_cache - make a binary test program cache (set of tar files) for
# the indicated board.  This will build a cache of all tests.
#
# Usage: make_cache <board> [<tests>]

usage() {
    echo "Usage: make_cache [<options>] <board> [<test_name1> [<test_name2>...]]"
    echo
    echo "If no tests are specified, make binary packages for all tests."
    echo "The <test_name> can be a full test name, or a pattern, like"
    echo "'Functional.fuego_'. Shell pattern matching (that is: =~) is"
    echo "used to match test names to the pattern."
    echo
    echo "Use 'ftc list-tests' to get a list of tests installed on this system"
    echo
    echo "Options:"
    echo "-h = show this usage help"
    echo "-v = put verbose information to stdout while making packages"
    echo
    echo "Verbose logging is available in /fuego-rw/cache/cache.log, whether -v"
    echo "is used or not."
    exit 1
}

if [ -z "$1" ] ; then
    echo "Error: Missing arguments"
    usage
fi

# parse command line options
while [ $(echo -- $1 | cut -b 4) = "-" ] ; do
    case $1 in
        -h|--help) usage;;
        -v|--verbose)
            VERBOSE=true
            shift;;
        --) shift; break;;
        -*) echo "Invalid option: $1" ; usage ;;
    esac
done

if [ -z "$FUEGO_RW" ] ; then
    export FUEGO_RW=/fuego-rw
fi

# get board

if [ -z "$1" ] ; then
    echo "Error: Missing board"
    echo "Use -h for usage"
    exit 1
else
    board=$1
    shift
fi

if [ ${board#Benchmark} != ${board} ] ; then
    echo "Error: Probably incorrect board of '$board' specified"
    echo "Use -h for usage"
    exit 1
fi

if [ ${board#Functional} != ${board} ] ; then
    echo "Error: Probably incorrect board of '$board' specified"
    echo "Use -h for usage"
    exit 1
fi

candidate_tests=$(ftc -q list-tests)

# see if one or more tests was specified
if [ -z "$1" ] ; then
	tests="$candidate_tests"
else
    tests=""
    while [ -n "$1" ] ; do
        for item in $candidate_tests ; do
            if [[ $item =~ $1 ]] ; then
	            tests="$tests $item"
            fi
        done
        shift
    done
fi

echo "Making the following tests:"
echo $tests

mkdir -p $FUEGO_RW/cache
chown jenkins.jenkins $FUEGO_RW/cache
CACHE_LOG=$FUEGO_RW/cache/cache.log

#
# FIXTHIS - need to mark this as NO_BOARD_CONTACT
echo "=============================================" | tee -a $CACHE_LOG
date | tee -a $CACHE_LOG
for test in $tests ; do
    echo "Making cache package for $test" | tee -a $CACHE_LOG
    if [ "$VERBOSE" = "true" ] ; then
        ftc run-test -b $board -t $test -p "pbdm" | tee -a $CACHE_LOG
    else
        ftc run-test -b $board -t $test -p "pbdm" >> $CACHE_LOG
    fi
done
