#!/bin/bash
#
# poll_requests.sh - this is a simple script to do an infinite loop
# polling for requests (for my lab) on the fuego server. If a job
# is found, it is executed.
#
# edit the following values to modify the default behavior of poll_requests.sh
# WAIT_TIME, REMOVE_REQUESTS, BOARD

# uncomment this for debugging
#set -x

# wait time between polling, in seconds
WAIT_TIME=60

# remove requests from server (1=remove, 0=don't remove)
REMOVE_REQUESTS=0

# if no '-d' specified, run forever (no END_TIME)
END_TIME=""

# by default, do not allow ftc to upgrade the test from the server
UPGRADE_ARG=""

# if BOARD is set, only process requests for the indicated board
#BOARD="min1"

# argument to use to force debug output by ttc
# (set by command line argument)
DEBUG_ARG=""

usage() {
   cat <<HERE
Usage: poll_requests.sh [options]

Poll for requests from the fuego server (configured in fuego.conf).
Execute any requests found, and repeat.

Options:
  -h, --help    Show this usage help
  -w <seconds>  Specify waiting period between polling requests, in seconds
                  (default is 60 seconds)
  -r            Remove requests from the server when the request is completed
                  (default is to not remove completed requests)
  -b <board>    Specify a board to execute requests for.  If not specified,
                  execute requests for any board in the local system.
  -d <duration> Specify a duration to run for.  Consists of a number
                followed by a suffix (s,m,h,d).  poll_requests.sh will
                automatically stop after the specified duration.
                Examples: 30m, 2h, 12h.  Use "-w 1 -d 1s" to
                do a "one shot" check of requests on the server.
  -u            Allow test upgrades.  If our local machine doesn't
                have a test, or our local version of the test is less
                than the one on the server, then upgrade the test by
                installing the latest version from the test server.
  --debug       Show debug output for some ftc operations.
HERE
}

# takes duration string as $1, and returns end time in seconds since
# the epoch
process_duration() {
    # units is last character of duration string
    len=$(( ${#1} - 1 ))
    units="${1: -1}"
    count="${1:0:$len}"

    # check for valid number
    num_regex="^[0-9]+$"
    if ! [[ $count =~ $num_regex ]] ; then
        echo "Invalid duration '$DURATION' specified" >&2
        echo "count=$count (should be an integer number)" >&2
        echo "-1"
        return 1
    fi

    now=$(date +%s)
    if [ "$units" = "s" ] ; then
        echo $(( now + count ))
        return 0
    fi
    if [ "$units" = "m" ] ; then
        echo $(( now + count * 60 ))
        return 0
    fi
    if [ "$units" = "h" ] ; then
        echo $(( now + count * 3600 ))
        return 0
    fi
    if [ "$units" = "d" ] ; then
        echo $(( now + count * 86400 ))
        return 0
    fi
    echo "Invalid duration '$DURATION' specified" >&2
    echo "units=$units (should be one of s,m,h,d)" >&2
    echo "count=$count (should be an integer number)" >&2
    echo "-1"
    return 1
}

while [ -n "$1" ]
do
    case $1 in
        -h|--help )
            usage
            exit 0
            ;;
        -w )
            shift ;
            if [ -z $1 ] ; then
                echo "Error: Missing wait time for -w option"
                echo "Use -h for help" ;
                exit 1
            fi
            WAIT_TIME="$1" ;
            shift ;
            ;;
        -r )
            shift ;
            REMOVE_REQUESTS=1
            ;;
        -b )
           shift ;
           BOARD="$1" ;
           shift ;
           ;;
        -u )
           shift ;
           UPGRADE_ARG="--allow-upgrade" ;
           ;;
        -d )
           shift ;
           DURATION=$1
           END_TIME=$(process_duration $DURATION)
           if [ "${END_TIME}" = "-1" ] ; then
               exit 1
           fi
           shift ;
           ;;
        --debug )
           shift ;
           DEBUG_ARG="--debug"
           ;;
        * )
           echo "Error: unrecognized option \"$1\"" ;
           echo "Use -h for help" ;
           exit 1 ;
     esac
done

# set BOARD_ARG from BOARD
if [ -n "${BOARD}" ] ; then
    BOARD_ARG="board=$BOARD"
else
    BOARD_ARG=""
fi

# only check for jobs for my host
HOST=$(ftc config host)
HOST_ARG="host=$HOST"


server_type=$(ftc config server_type)
server_domain=$(ftc config server_domain)
if [ "$server_type" == "fuego" -a -n "$server_domain" ] ; then
    echo "== Polling server \"$server_domain\" for requests =="
    if [ -n "$BOARD_ARG" ] ; then
        echo "Processing requests for $BOARD_ARG"
    else
        echo "Processing requests for any board"
    fi
else
    echo "Error: fuego.conf does not have a valid configuration for Fuego server"
    echo "Please add a Fuego server to fuego.conf in order to poll for requests."
    exit 1
fi

# check to see that my host has at least one board registered with fserver
is_there=$(ftc list-boards -r | grep -q "$HOST:" ; echo $?)
if [ $is_there != "0" ] ; then
    echo "Error: no boards for this host ($HOST) are registered with the server"
    echo "Check the boards on the server with 'ftc list-boards -r'"
    echo "It makes no sense to poll for requests."
    echo "Please register a board with the fserver, and try again."
    echo "Alternatives, check your fserver configuration in fuego.conf"
    exit 1
fi

# emit message about allowing upgrades
if [ -n "$UPGRADE_ARG" ] ; then
    echo "Installing new tests or upgrading tests is allowed."
fi

# emit message about wait times
echo "Waiting $WAIT_TIME seconds between polls of the server."

# emit message about duration
if [ -z "$END_TIME" ] ; then
    echo "Running forever (no specified duration)"
else
    echo "Running for $DURATION"
fi

echo "Type Ctrl-C to exit"

while true ; do
    echo -n "Checking "
    request_id="$(ftc list-requests -q ${HOST_ARG} ${BOARD_ARG} state=pending | head -n 1)"
    if [ -n "${request_id}" ] ; then
        echo
        echo "Running request: $request_id"
        ftc run-request $DEBUG_ARG --put-run $UPGRADE_ARG $request_id ;
        if [ "$REMOVE_REQUESTS" = "1" ] ; then
            ftc rm-request $request_id ;
        fi
    else
        echo -n "Waiting "
        for i in $(seq $WAIT_TIME) ; do
            echo -n "."
            sleep 1
        done
        echo
    fi
    # check for end of polling time
    if [ -n "$END_TIME" ] ; then
        now=$(date +%s)
        if [ $now -ge $END_TIME ] ; then
            echo "Done - polling duration of $DURATION is completed"
            exit 0
        fi
    fi
done
