#!/bin/bash
# vim: ts=4 sw=4 et :
# populate_board_cache - populate the dependency cache for a board
#  use the pre-check phase, and set CACHE_DEPENDENCIES
# the indicated board.  This will build a cache of all tests.
#
# Usage: populate_board_cache.sh <board> <test list>

function usage() {
    echo "Usage: populate_board_cache.sh <board> [test-name1[,...]]"
    echo
    echo "-h = show this usage help"
    echo "-v = show verbose output"
    exit 1
}

function vprint() {
    if [ "$PBC_VERBOSE" = 1 ] ; then
        echo "$@"
    fi
}

if [ -z "$1" ] ; then
    echo "Error: Missing arguments"
    usage
fi

unset PBC_VERBOSE
# parse command line options
while [ $(echo -- $1 | cut -b 4) = "-" ] ; do
    case $1 in
        -h|--help) usage ;;
        -v|--verbose) shift ; export PBC_VERBOSE=1 ;;
        --) shift; break ;;
        -*) echo "Invalid option: $1" ; usage ;;
    esac
done

# get board
board=$1
shift
if [ -z "$board" ] ; then
    echo "Error: Missing board"
    usage
fi

FUEGO_CORE="$(ftc config fuego_core_dir)"

tests=$@
if [ -z "$tests" ] ; then
    echo "NOTE: missing list of tests, getting dependencies for all installed tests"
    tests=$(ftc -q list-tests | grep -v Functional.fuego_dep_cache)
fi

echo "Clearing dependency cache for board '$board'"
cached_progs=$(ftc query-board -b $board -q | grep PROGRAM_ | tr '\n' ' ')
for prog in ${cached_progs} ; do
    echo "  clearing $prog from cache"
    ftc delete-var -b $board $prog
done

# accumulate a list of programs to check for
echo "Finding program dependencies for tests"
for test in $tests ; do
    # parse assert_has_program lines
    test_script=$FUEGO_CORE/tests/$test/fuego_test.sh
    if [ -f $test_script ] ; then
        prog="$(grep ' assert_has_program ' $FUEGO_CORE/tests/$test/fuego_test.sh | sed s/.*assert_has_program\ // | tr '\n' ' ')"
        if [ -n "${prog}" ] ; then
            vprint "  For test $test, programs to check for are: $prog"
            prog_list="$prog_list $prog"
        fi
    else
	echo "Error: test '$test' not found."
    fi
done

echo "Programs to check for: $prog_list"

# $1 = board
# $2 = program to check for
function update_cache_for_program() {
    local board=$1
    up_name=${2^^}
    prog_var=PROGRAM_${up_name//[-,.]/_}

    echo "  checking board $board for program $2"
    LOCATION=$(./board-cmd.sh $board "command -v $2")
    if [ -z "$LOCATION" ] ; then
        LOCATION="<missing>"
    fi
    echo "    location=$LOCATION"
    ftc set-var -b $board $prog_var=$LOCATION
}

# now check for these programs
# let's start by doing it one at a time
for prog in ${prog_list} ; do
    update_cache_for_program $board $prog
done
