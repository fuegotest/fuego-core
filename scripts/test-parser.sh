#!/bin/bash
#
# test-parser.sh - re-run the parser for the indicated run
#
# get test & runlog (board.spec.#.#) from command args
#

if [ $1 = "-h" -o $1 = "--help" ] ; then
    echo "Usage test-parser.sh <test> <run-log-id>"
    echo "example: test-parser.sh Functional.hello_world min1.default.5.5"
    echo "Options:"
    echo " -h       Show this usage help"
    echo " --debug  Show debug info while running"
    echo ""
    echo "Note: This should be run inside the Fuego docker container"
    exit 0
fi

if [ $1 = "--debug" ] ; then
    set -x
    shift
    export FUEGO_LOGLEVELS="parser:debug,criteria:debug,charting:debug"
fi

if [ -z $1 ] ; then
    echo "Error: Missing test name (use -h for usage help)"
    exit 127
fi
TESTDIR=$1
shift

if [ -z $1 ] ; then
    echo "Error: Missing runlog id (use -h for usage help)"
    exit 127
fi
RUNLOG=$1

export LOGDIR=/fuego-rw/logs/$TESTDIR/$RUNLOG
SNAPSHOT=$LOGDIR/machine-snapshot.txt

if [ ! -f $SNAPSHOT ] ; then
    echo "Error: Missing file $SNAPSHOT (possible 'test' or 'run-log-id' error?)"
    exit 127
fi

# Read test variables into environment vars, from $SNAPSHOT file
ENV_LIST="FUEGO_RW FUEGO_RO FUEGO_CORE NODE_NAME TESTDIR TEST_HOME \
    TESTSPEC BUILD_NUMBER BUILD_ID BUILD_TIMESTAMP \
    TOOLCHAIN FWVER LOGDIR FUEGO_START_TIME Reboot \
    Rebuild Target_PreCleanup WORKSPACE JOB_NAME \
    FUEGO_VERSION FUEGO_CORE_VERSION TESTSUITE_VERSION \
    Target_PostCleanup"

for v in $ENV_LIST ; do
    line=$(grep "^${v}=" $SNAPSHOT)
    val=${line#*=}
    export $v="$val"
done

# FIXTHIS - assume test worked? (need to pass this in, or emit it somewhere I can get it)
export RETURN_VALUE=0
# Ideas:
# check run.json and if test PASS, then assume RETURN_VALUE=0
# if test FAIL, then assume RETURN_VALUE=123
# if test ERROR, then unset RETURN_VALUE

# calculate parser
PARSER=$TEST_HOME/parser.py
if [ ! -f $PARSER ] ; then
    PARSER=$FUEGO_CORE/scripts/generic_parser.py
fi

PY_EXE=/usr/bin/python
export PYTHONPATH=/fuego-core/scripts/parser:
TOOLCHAIN=$TOOLCHAIN $PY_EXE -W ignore::DeprecationWarning -W ignore::UserWarning $PARSER

