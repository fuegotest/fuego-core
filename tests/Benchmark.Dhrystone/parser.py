#!/usr/bin/python

import os, re, sys
import common as plib

regex_string = "^(Microseconds.for.one.run.through.Dhrystone:|Dhrystones.per.Second:)(\ *)([\d]{1,8}.?[\d]{1,3})(.*)$"
measurements = {}

matches = plib.parse_log(regex_string)

if matches:
    measurements['default.Dhrystone'] = [{"name": "microseconds_for_one_run", "measure" : float(matches[0][2])},{"name": "Score", "measure" : float(matches[1][2])}]

sys.exit(plib.process(measurements))
