tarball=bonnie++-1.03e.tar.gz
# FIXTHIS - upgrade bonnie++ to version 1.97.3 (or 1.04)
# See https://doc.coker.com.au/projects/bonnie/
# and https://www.coker.com.au/bonnie++/

function test_pre_check {
    assert_define BENCHMARK_BONNIE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_BONNIE_MOUNT_POINT
    assert_define BENCHMARK_BONNIE_SIZE
    assert_define BENCHMARK_BONNIE_ROOT
    is_on_target_path bonnie\+\+ PROGRAM_BONNIE
}

function test_build {
    if [ -z "$PROGRAM_BONNIE" ]; then
        # default to static build, unless overridden by build variable
        LFLAGS="-static"
        if [[ "${FUEGO_BUILD_FLAGS}" == *no_static* ]] ; then
            LFLAGS=""
        fi

        patch <$TEST_HOME/fix-nogroup-gid-if.patch
        # uncomment to change the number of seeks for the Random Seek test
        #patch -p1 <$TEST_HOME/bonnie-seeks.patch
        ./configure --host=$HOST --build=`uname -m`-linux-gnu;
        LFLAGS="$LFLAGS" make
    else
        echo "Skipping build phase, bonnie++ is already on the target"
    fi
}

function test_deploy {
    if [ -z "$PROGRAM_BONNIE" ]; then
        put bonnie++  $BOARD_TESTDIR/fuego.$TESTDIR/
    fi
}

function test_run {
    if [ -z "$BENCHMARK_BONNIE_RAM" ] ; then
        BENCHMARK_BONNIE_RAM=0
    fi

    if [ -z "$BENCHMARK_BONNIE_NUM_FILES" ] ; then
        BENCHMARK_BONNIE_NUM_FILES="16:0:0:1"
    fi

    hd_test_mount_prepare $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT

    if [ "$BENCHMARK_BONNIE_ROOT" == "true" ]; then
        BONNIE_ROOT_PARAM="-u 0:0"
    else
        BONNIE_ROOT_PARAM=""
    fi

    get_program_path "bonnie++"
    report "pwd; ls; $PROGRAM_BONNIE__ -d $BENCHMARK_BONNIE_MOUNT_POINT/fuego.$TESTDIR $BONNIE_ROOT_PARAM -s $BENCHMARK_BONNIE_SIZE -r $BENCHMARK_BONNIE_RAM -n $BENCHMARK_BONNIE_NUM_FILES -m $NODE_NAME"

    sync

    hd_test_clean_umount $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT
}
