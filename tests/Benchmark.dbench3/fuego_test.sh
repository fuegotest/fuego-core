tarball=dbench-3.04.tar.gz

function test_pre_check {
    assert_define BENCHMARK_DBENCH3_MOUNT_BLOCKDEV
    assert_define BENCHMARK_DBENCH3_MOUNT_POINT
    assert_define BENCHMARK_DBENCH3_TIMELIMIT
    assert_define BENCHMARK_DBENCH3_NPROCS
}

function test_build {
    patch -N -s -p1 < $TEST_HOME/dbench_startup.patch
    ./configure --host=$HOST --build=`uname -m`-linux-gnu  CFLAGS="$CFLAGS";
    make
}

function test_deploy {
    put dbench client.txt $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    hd_test_mount_prepare $BENCHMARK_DBENCH3_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH3_MOUNT_POINT

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        cp client.txt $BENCHMARK_DBENCH3_MOUNT_POINT/fuego.$TESTDIR; \
        pwd; ./dbench -t $BENCHMARK_DBENCH3_TIMELIMIT \
        -D $BENCHMARK_DBENCH3_MOUNT_POINT/fuego.$TESTDIR \
        -c $BENCHMARK_DBENCH3_MOUNT_POINT/fuego.$TESTDIR/client.txt \
        $BENCHMARK_DBENCH3_NPROCS; \
        rm $BENCHMARK_DBENCH3_MOUNT_POINT/fuego.$TESTDIR/client.txt; \
        sync"

    hd_test_clean_umount $BENCHMARK_DBENCH3_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH3_MOUNT_POINT
}
