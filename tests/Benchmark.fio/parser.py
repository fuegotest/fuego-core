#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys
import common as plib

test_version = ''
with open(plib.TEST_LOG,'r') as cur_file:
    raw_values = cur_file.readlines()
    test_version = raw_values[4].rstrip("\n")

if test_version > '2.16':
    regex_string = '( READ:| WRITE:).*bw=.*\(([\d.]+)([kKM]B\/s)\)'
else:
    regex_string = '(  READ:|  WRITE:).*aggrb=([\d.]+)([KM]B\/s)'

measurements = {}

# handle results in MB or KB
def set_measure(tguid, match):
    speed = match[1]
    units = match[2]
    multiplier = 1
    if units.startswith('M'):
        # WARNING - this depends on the value of kb_base specified to
        # the tool.  Currently, fuego_test.sh doesn't specify one,
        # so kb_base should default to 1024. (not 1000)
        multiplier = 1024

    measure = float(speed)*multiplier
    measurements[tguid] = [{"name": "speed",
        "measure" : '%.2f' % measure}]

matches = plib.parse_log(regex_string)
if matches:
    set_measure('Read.Seq', matches[0])
    set_measure('Read.Random', matches[1])
    set_measure('Write.Seq', matches[2])
    set_measure('Write.Random', matches[3])
    set_measure('File.Read', matches[4])
    set_measure('File.Write', matches[5])

sys.exit(plib.process(measurements))
