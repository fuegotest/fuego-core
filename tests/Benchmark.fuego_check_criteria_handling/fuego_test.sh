function test_run {
    if [ $TESTSPEC = "default" ] ; then
        # work around a bug with nested invocations.  The nested calls
        # to 'ftc run-test' will wipe out the 'before' system log.
        # Save it off before that happens, and restore it after
        # the nested tests.
        local fuego_test_tmp="${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR"
        local syslog_before="${fuego_test_tmp}/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.before"

        # NOTE: this approach doesn't handle test concurrency very well
        # multiple tests running nested tests could overwrite syslog_before.save
        # But I don't care at the moment.
        cmd "cp ${syslog_before} /tmp/syslog_before.save"
    fi

    log_this "echo Starting test"
    TC_NUM=$BENCHMARK_FUEGO_CHECK_CRITERIA_HANDLING_TC_NUM
    NUMBER=$BENCHMARK_FUEGO_CHECK_CRITERIA_HANDLING_NUMBER
    if [ $TESTSPEC = "gen_results" ] ; then
        log_this "echo Generating one data point"
        log_this "echo m$TC_NUM=$NUMBER"
    else
        log_this "echo Running sub-tests..."
        log_this "echo All reference values are 5, except for range2"
        log_this "echo and 29, 30 and 31, which are zero-checks"

        # this is a bit tricky
        export FUEGO_CALLER="nested"
        while read -r line ; do
            echo "... $line ..."
            set -- $line
            tc_num=$1
            tc_name=$2
            num=$3

            set +e
            ftc run-test -b $NODE_NAME \
              -t Benchmark.fuego_check_criteria_handling \
              -s gen_results \
              --dynamic-vars "{'NUMBER': '$num', 'TC_NUM': '$tc_num'}" ; RCODE=$?
            set -e

            log_this "echo $tc_name=$RCODE"
        done <<TESTCASE_LIST
01 measure_01_lt_check_low 1
02 measure_02_lt_check_hit 5
03 measure_03_lt_check_high 10
04 measure_04_le_check_low 1
05 measure_05_le_check_hit 5
06 measure_06_le_check_high 10
07 measure_07_gt_check_low 1
08 measure_08_gt_check_hit 5
09 measure_09_gt_check_high 10
10 measure_10_ge_check_low 1
11 measure_11_ge_check_hit 5
12 measure_12_ge_check_high 10
13 measure_13_eq_check_low 1
14 measure_14_eq_check_hit 5
15 measure_15_eq_check_high 10
16 measure_16_ne_check_low 1
17 measure_17_ne_check_hit 5
18 measure_18_ne_check_high 10
19 measure_19_bt_check_low 1
20 measure_20_bt_check_hit1 3
21 measure_21_bt_check_mid 5
22 measure_22_bt_check_hit2 7
23 measure_23_bt_check_high 10
24 measure_24_bt_check_low -10
25 measure_25_bt_check_hit1 -7
26 measure_26_bt_check_mid -5
27 measure_27_bt_check_hit2 -3
28 measure_28_bt_check_high -1
29 measure_29_eq_check_one_v_zero 1
30 measure_30_eq_check_zero_v_zero 0
31 measure_31_eq_check_zero_v_one 0
TESTCASE_LIST
    fi
    log_this "echo $TESTSPEC Done!"

    if [ $TESTSPEC = "default" ] ; then
        # re-create target log dir, and restore the 'before' syslog dump
        cmd "mkdir -p ${fuego_test_tmp}"
        cmd "mv -f /tmp/syslog_before.save ${syslog_before}"
    fi
}
