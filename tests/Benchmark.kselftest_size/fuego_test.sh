function test_build {
    make ARCH=$ARCHITECTURE defconfig
    make ARCH=$ARCHITECTURE headers_install

    # override kselftest lib.mk override of CC
    # we have some CC definitions in Fuego that are complex
    perl -p -i -e "s/^CC :=/#CC :=/" tools/testing/selftests/lib.mk

    make ARCH=$ARCHITECTURE TARGETS="size" -C tools/testing/selftests

    # Note: You can control the install path with this sequence:
    # cd tools/testing/selftests
    # mkdir -p fuego
    # KSFT_INSTALL_PATH=fuego make ARCH=$ARCHITECTURE TARGETS="size" install
    # I'm not sure when KSFT_INSTALL_PATH was introduced
    # I think the kselftest 'install' target was introduced in 4.1

    make ARCH=$ARCHITECTURE TARGETS="size" -C tools/testing/selftests install
    # this should leave stuff in tools/testing/selftests/kselftest_install
    # FIXTHIS - should test whether that works with older kernels

    # maybe this works for older kernels:
    # cd tools/testing/selftests
    # INSTALL_PATH=fuego make install
}

function test_deploy {
    put tools/testing/selftests/kselftest_install/size/get_size \
        $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # The sync and drop_caches operations may have already been performed,
    #   but likely not as close to the get_size operation as here
    # Note that this will disturb the performance of the running system
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sync ; echo 3 >/proc/sys/vm/drop_caches ; ./get_size"
}
