#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys
import common as plib

regex_string = ' (Buffer|In use): (\d.+)'
measurements = {}

# set_measure expects a python re.match object
# with a list of items that matched per line
# match[0] should be the field name
# match[1] should be the field value
def set_measure(tguid, match):
    name = match[0].replace(" ","_")
    measure = int(match[1])
    measurements[tguid] = [{"name": name,
        "measure" : '%d' % measure}]

match_lines = plib.parse_log(regex_string)
for match in match_lines:
    set_measure("default.size", match)

sys.exit(plib.process(measurements))
