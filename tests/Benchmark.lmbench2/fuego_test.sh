tarball=lmbench3.tar.gz

function test_build {
   mkdir -p SCCS
   touch SCCS/s.ChangeSet
   cd scripts
   patch -p0 < $TEST_HOME/lmbench3.config-run.patch
   patch -p0 < $TEST_HOME/lmbench.patch
   patch -p0 < $TEST_HOME/lmbench3.mem64.patch
   cd ../src
   patch -p0 < $TEST_HOME/bench.h.patch
   patch -p0 < $TEST_HOME/fix-undefined-reference-llseek.patch
   cd ..
   patch -p1 < $TEST_HOME/fix-build-param-quoting.patch
   CFLAGS+=" -g -O"
   export OS=$PREFIX
   LMBENCH_OS_STR=$(scripts/os)
   make OS="$LMBENCH_OS_STR" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS"
}

function test_deploy {
   put *  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
   # Get the scripts and results directory paths of lmbench
   if [ -z "$BENCHMARK_LMBENCH2_SCRIPTS_DIR" ]; then
      SCRIPTS_DIR="$BOARD_TESTDIR/fuego.$TESTDIR/scripts"
      RESULTS_DIR="$BOARD_TESTDIR/fuego.$TESTDIR/results"
      if cmd "test ! -d $SCRIPTS_DIR" ; then
         SCRIPTS_DIR=/usr/lib/lmbench/scripts
         RESULTS_DIR=/var/lib/lmbench/results
         if cmd "test ! -d $SCRIPTS_DIR" ; then
            abort_job "Could not find lmbench scripts directory. Maybe specify SCRIPTS_DIR dynamic variable?"
         fi
      fi
   else
      SCRIPTS_DIR="$BENCHMARK_LMBENCH2_SCRIPTS_DIR"
      if [ -z "$BENCHMARK_LMBENCH2_RESULTS_DIR" ]; then
         RESULTS_DIR="$BOARD_TESTDIR/fuego.$TESTDIR/results"
      else
         RESULTS_DIR="$BENCHMARK_LMBENCH2_SCRIPTS_DIR"
      fi
   fi

   # Use some trickery to get the LMBENCH_OS_STR
   if [ -n "$PREFIX" ] ; then
      LMBENCH_OS_STR=$(cmd ls $SCRIPTS_DIR/../bin)
   else
      get_program_path os $SCRIPTS_DIR:$BOARD_TESTDIR/fuego.$TESTDIR/scripts/os
      LMBENCH_OS_STR=$(cmd "$PROGRAM_OS")
   fi

   cmd "rm -rf $RESULTS_DIR/*"
   cmd "cd $SCRIPTS_DIR; OS=$LMBENCH_OS_STR BINDIR=$SCRIPTS_DIR/.. ./config-run"
   cmd "cd $SCRIPTS_DIR; OS=$LMBENCH_OS_STR RESULTSDIR=$RESULTS_DIR ./results"
   report "cd $SCRIPTS_DIR; ./getsummary $RESULTS_DIR/$LMBENCH_OS_STR/*.0"
}

function test_cleanup {
   kill_procs lmbench lat_mem_rd par_mem
}
