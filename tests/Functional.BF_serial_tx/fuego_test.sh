#FUEGO_DEBUG=1

# Make sure that:
#
# -"getty" is not running on the DUT's port under test.
# - the latest 'lc' is in the docker container
# - the labcontrol server is running
function test_pre_check {
    RATES=${BAUD_RATES:-${FUNCTIONAL_BF_SERIAL_TX_BAUDRATES}}
    if [ -z "$RATES" ] ; then
        abort_job "Missing BAUD_RATES - need to define in board file or spec file"
    fi

    # serial ports for test should be defined in the board file
    assert_define SERIAL_ID
    assert_define SERIAL_DEV

    assert_has_program stty
}

# show a TAP-formatted failuer line
# $1 = TESTCASE name
# $2 = optional diagnostic data
function fail {
    if [ -n "$2" ] ; then
        log_this "echo \"# $2\""
    fi
    log_this "echo \"not ok $TC_NUM - $1\""
    TC_NUM="$(( $TC_NUM + 1 ))"
}

# show a TAP-formatted success line
# $1 = TESTCASE name
# $2 = optional diagnostic data
function succeed {
    if [ -n "$2" ] ; then
        log_this "echo \"# $2\""
    fi
    log_this "echo \"ok $TC_NUM - $1\""
    TC_NUM="$(( $TC_NUM + 1 ))"
}


function test_one_rate {
    BAUD_RATE=$1

    TESTCASE="Check transmission at baud-rate $BAUD_RATE"
    echo "  Configuring for baud rate $BAUD_RATE"

    set +e
    # Configure baud rate for DUT, then lab endpoint
    cmd "stty -F $SERIAL_DEV $BAUD_RATE raw -echo -echoe -echok"
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not set baud rate $BAUD_RATE for $BOARD_NAME:$SERIAL_ID"
        return
    fi

    echo "{ \"baud_rate\": \"$BAUD_RATE\" }" | \
        $CLIENT $RESOURCE set-config serial >/dev/null 2>&1
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not set baud rate $BAUD_RATE for $RESOURCE"
        return
    fi

    echo "  Capturing data at lab resource $RESOURCE"
    TOKEN="$($CLIENT $RESOURCE serial start)"
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not start serial capture with $RESOURCE"
        return
    fi

    # use for debugging
    #echo "capture token=$TOKEN"

    # give time for receiver to fully settle - I'm not sure this is needed
    sleep 0.5

    echo "  Transmitting data from DUT"
    cmd "echo -n \"$SEND_DATA\" >$SERIAL_DEV"

    #echo "  Stopping serial capture"
    echo -n "  "

    $CLIENT $RESOURCE serial stop $TOKEN
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not stop serial capture"
        return
    fi

    echo "  Getting captured data"

    RECEIVED_DATA="$($CLIENT $RESOURCE serial get-data $TOKEN)"
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not get serial data"
        return
    fi

    #echo "  Deleting the data on the server"

    $CLIENT $RESOURCE serial delete $TOKEN >/dev/null 2>&1 || \
        echo "Warning: Could not delete data on server"

    #echo "SEND_DATA=$SEND_DATA"
    #echo "RECIEVED_DATA=$RECEIVED_DATA"

    # now actually compare the data to get the testcase result
    if [ "$SEND_DATA" != "$RECEIVED_DATA" ] ; then
        log_this "echo \"# sent data: $SEND_DATA\""
        log_this "echo \"# received data: $RECEIVED_DATA\""
        fail "$TESTCASE" "Received data does not match sent data"
        return
    else
        succeed "$TESTCASE"
        return
    fi
}

function test_run {
    CLIENT="lc"

    RATES=${BAUD_RATES:-${FUNCTIONAL_BF_SERIAL_TX_BAUDRATES}}
    RATE_COUNT=$(echo $RATES | wc -w)

    echo "Getting resource and lab endpoint for board"
    export BOARD_NAME=${BF_NAME:-${NODE_NAME}}

    echo "$CLIENT $BOARD_NAME get-resource serial $SERIAL_ID"
    RESOURCE=$($CLIENT $BOARD_NAME get-resource serial $SERIAL_ID)
    echo "RESOURCE=$RESOURCE"

    SEND_DATA="This is an ASCII test string, transmitted over the serial line"

    echo "Starting serial transmission test"
    report "echo TAP version 13"
    log_this "echo 1..$RATE_COUNT"

    export TC_NUM=1

    for RATE in $RATES ; do
        test_one_rate $RATE
    done

    log_this "echo Done."
}

#function test_processing {
#    log_compare "$TESTDIR" "$(echo "$BAUD_RATES" | wc -w)" "^ok" "p"
#}
