===========
Description
===========

Obtained from addkey02.c DESCRIPTION:
	Test that the add_key() syscall correctly handles a NULL payload with nonzero
	length.  Specifically, it should fail with EFAULT rather than oopsing the
	kernel with a NULL pointer dereference or failing with EINVAL, as it did
	before (depending on the key type).  This is a regression test for commit
	5649645d725c ("KEYS: fix dereferencing NULL payload with nonzero length").
	
	Note that none of the key types that exhibited the NULL pointer dereference
	are guaranteed to be built into the kernel, so we just test as many as we
	can, in the hope of catching one.  We also test with the "user" key type for
	good measure, although it was one of the types that failed with EINVAL rather
	than dereferencing NULL.
	
	This has been assigned CVE-2017-15274.

Other notes:
	Commit 5649645d725c appears to have been included since the kernel 4.12.

====
Tags
====

* kernel, syscall, addkey

=========
Resources
=========

* https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=5649645d725c73df4302428ee4e02c869248b4c5

=======
Results
=======

Running on a PC (64 bits) using Debian Jessie (kernel 3.16):
  add_key02.c:81: CONF: kernel doesn't support key type 'asymmetric'
  add_key02.c:81: CONF: kernel doesn't support key type 'cifs.idmap'
  add_key02.c:81: CONF: kernel doesn't support key type 'cifs.spnego'
  add_key02.c:81: CONF: kernel doesn't support key type 'pkcs7_test'
  add_key02.c:81: CONF: kernel doesn't support key type 'rxrpc'
  add_key02.c:81: CONF: kernel doesn't support key type 'rxrpc_s'
  add_key02.c:96: FAIL: unexpected error with key type 'user': EINVAL
  add_key02.c:96: FAIL: unexpected error with key type 'logon': EINVAL

The kernel should have returned an EFAULT error, not EINVAL:
  Ref: https://github.com/linux-test-project/ltp/issues/182

.. fuego_result_list::

======
Status
======

.. fuego_status::

=====
Notes
=====
