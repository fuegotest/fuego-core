# some functions are shared between Benchmark.OpenSSL and Functional.OpenSSL
tarball=../OpenSSL/openssl-1.0.0t.tar.gz
source $FUEGO_CORE/tests/OpenSSL/openssl.sh

function test_deploy {
    put apps util test run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_processing {
    P_CRIT="passed|ok"

    log_compare "$TESTDIR" "169" "${P_CRIT}" "p"
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; bash run-tests.sh"
}


