#!/bin/sh

#  In target, start the acpid and check if the Unix domain socket is created or not.

test="socketfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target acpid stop
if [ -f /var/run/acpid.socket ]
then
    rm -f /var/run/acpid.socket
fi

if exec_service_on_target acpid start
then
    echo " -> start of acpid succeeded."
else
    echo " -> start of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/run/acpid.socket
then
    echo " -> ls /var/run/acpid.socket succeeded."
else
    echo " -> ls /var/run/acpid.socket failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target acpid stop
then
    echo " -> stop of acpid succeeded."
else
    echo " -> stop of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if test -f /var/run/acpid.socket
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
