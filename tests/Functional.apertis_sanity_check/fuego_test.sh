function test_build {
    ### get current branch in apertis-testcases repo
    BRANCH=$(git branch | grep ^[*] | cut -b 3-)
    echo BRANCH=$BRANCH

    # get apertis-tests
    git clone -b $BRANCH https://gitlab.apertis.org/pkg/apertis-tests.git

    # create apertis-tests.tgz
    tar -cf apertis-tests.tgz --exclude=apertis-tests/.git* apertis-tests

    # create sanity-check-run-steps.sh script file
    SCRIPT_FILE=sanity-check-run-steps.sh
    $TEST_HOME/make-script-from-yaml.py test-cases/sanity-check.yaml >$SCRIPT_FILE
    chmod a+x $SCRIPT_FILE
}

function test_deploy {
    put apertis-tests.tgz $BOARD_TESTDIR/fuego.$TESTDIR/
    put sanity-check-run-steps.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR ; tar -xf apertis-tests.tgz"
    report "./sanity-check-run-steps.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST_RESULT:fail:" "n"
}
