#!/usr/bin/python
#
# make-script-from-yaml:
# Create shell script from certain lines in a yaml file
# the lines are similar to the following
# run:
#   steps:
#    - "# Enter test directory:"
#    - cd apertis-tests
#    - "# Execute the following command:"
#    - common/sanity-check
#

import yaml
import sys
import os

def error_out(msg):
    print("Error: " + msg)
    sys.exit(1)

def usage(rcode):
    print("""Usage: make-script-from-yaml <yaml-file>
Create a shell script from the list of lines under the 'run.steps'
attribute of the specified yaml file.  The shell script is written
to stdout.""")
    sys.exit(rcode)

try:
    yaml_filename = sys.argv[1]
except:
    error_out("Missing filename to parse")

f = open(yaml_filename, "r")
data = yaml.safe_load(f)
lines = data["run"]["steps"]

print("#!/bin/sh")
print("# run.steps from %s" % os.path.basename(yaml_filename))
print("")

for line in lines:
    print(line)

