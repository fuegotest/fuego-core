#
# user must define in the board file:
#  ARCH_TIMER_NAME = the name in /proc/interrupts for the timer interrupt
#  ARCH_TIMER_ID = the number (or id) in /proc/interrupts for the interrupt
#
# Optionally, the user MAY define in the board file:
#  ARCH_TIMER_STRING = a string in dmesg showing initialization for the timer
#
function test_pre_check {
    assert_define ARCH_TIMER_NAME "Please define in board file as name of timer interrupt"
    assert_define ARCH_TIMER_ID "Please define in board file as number or id of timer interrupt"
}

function test_deploy {
    put $TEST_HOME/check_arch_timer.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "$BOARD_TESTDIR/fuego.$TESTDIR/check_arch_timer.sh \
        \"$ARCH_TIMER_NAME\" \"$ARCH_TIMER_ID\" \"$ARCH_TIMER_STRING\""
}

function test_processing {
    log_compare "$TESTDIR" 0 "^not ok" "n"
}

