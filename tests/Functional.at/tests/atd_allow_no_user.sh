#!/bin/sh

#  In the target start atd, check the operation if no user .

test="allow_no_user"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target atd stop

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

cp data/at_no.allow /etc/at.allow

useradd test_for_fuego
cp data/test_add.sh /home/test_for_fuego/

su - test_for_fuego -c "at -f /home/test_for_fuego/test_add.sh now + 5 minutes || echo error" > log 2>&1
if cat log | grep "error"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log

sleep 5

rm -fr /home/test_for_fuego
userdel -f test_for_fuego
rm -f /etc/at.allow
at -d $(at -l | cut -b 1-2)

exec_service_on_target atd stop
