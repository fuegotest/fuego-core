function test_pre_check {
    assert_has_program atmtcp
    assert_has_program lsmod
    assert_has_program modprobe
    assert_has_module atmtcp
}

function test_deploy {
    put $TEST_HOME/atmtcp_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
        ./atmtcp_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
