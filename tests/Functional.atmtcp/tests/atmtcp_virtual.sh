#!/bin/sh

#  In target, run command atmtcp.
#  option: virtual

test="virtual"

if lsmod | grep atmtcp
then
    echo " -> $test: atmtcp is already loaded."
else
    if modprobe -v atmtcp
    then
        echo " -> $test: load atmtcp succeeded."
    else
        echo " -> $test: load atmtcp failed."
        echo " -> $test: TEST-FAIL"
        exit
    fi
fi

if [ ! -n "$(atmtcp virtual connect 127.0.0.1 | grep "virtual interface 0")" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

killall atmtcp
exit 0
