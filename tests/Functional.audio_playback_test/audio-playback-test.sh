#!/bin/sh
#
# audio-play-test.sh - use lc to do an audio play test
#
# This is a very simple test, sending data from the DUT audio output
# to an audio input resource in the lab
#
# outline:
#  discover lab endpoint for DUT audio connection
#  - specify configuration of DUT audio device
#  start capture for lab endpoint (microphone input)
#  write data to DUT audio device (ie play something)
#  get data from lab capture
#    trim start and end of captured data
#  compare data - test success or failure (FIXTHIS - needs work)
#  delete lab capture and temp files
#

# can set this to LOGDIR to save files, if you remove the cleanup code

TMPDIR=/tmp
if [ -n "$LOGDIR" ] ; then
    TMPDIR=$LOGDIR
fi

start_time=$(date +"%s.%N")

error_out() {
    echo "Error: $@"
    exit 1
}

# Try to auto-detect client.
# Caller can specify CLIENT in environment variable.
# The client should be one of: 'lc' or 'ebf'
if [ -z "$CLIENT" ] ; then
    CLIENT=$(command -v lc)
    if [ -z "$CLIENT" ] ; then
        CLIENT=$(command -v ebf)
    fi
fi

if [ -z "$CLIENT" ] ; then
    error_out "Cannot find board farm client"
fi

echo "Using client '$CLIENT'"

# parse arguments
if [ "$1" = "-h" -o "$1" = "--help" ] ; then
    cat <<HERE
Usage: audio-play-test.sh [-h] [-v] [--noremove] <board> <device> <device_id> <audio-file>

Play data to the audio output of a board, and receive it in the lab.
Arguments:
  <board>      Board name registered with lab server
  <device>     Audio device for output under test (e.g. 'default', 'plughw:...')
  <device_id>  String identifier for audio output under test (e.g. usb-audio)
               This is used to find the lab endpoint for the audio connection.
  <audio-file> A file to play for the test

Options:
 -h, --help  Show this usage help
 -v          Show verbose output
 --noremove  Leave temp files (usually in the log directory)

Test output is in TAP format.  This test requires that a board farm
REST API client be installed on the board (e.g. 'lc' or 'ebf'), and that
the server configuration has previously been set up -- that is, the
client is logged in, the indicated board is reserved, audio output hardware
is set up, and the correct configuration for the audio receiver resource
and connection is present on the lab server.

The caller of this program can specify either 'lc' or 'ebf' as the client to the
farm, by setting CLIENT in the shell environment before executing this program.
HERE
    exit 0
fi

export VERBOSE=
if [ "$1" = "-v" ] ; then
    export VERBOSE=1
    shift
fi

if [ "$1" = "--noremove" ] ; then
    export NOREMOVE=1
    shift
fi

BOARD="$1"
if [ -z $BOARD ] ; then
    error_out "Missing board argument for audio test"
fi
shift

DEVICE="$1"
if [ -z $DEVICE ] ; then
    error_out "Missing device argument for audio test"
fi
shift

DEVICE_ID="$1"
if [ -z $DEVICE_ID ] ; then
    error_out "Missing device argument for audio test"
fi
shift

AUDIO_FILE="$1"
if [ -z $AUDIO_FILE ] ; then
    error_out "Missing audio file for audio test"
fi
shift

export TC_NUM=1

# show a TAP-formatted fail line
# $1 = TESTCASE name
# optional $2 = failure reason
fail() {
    if [ -n "$2" ] ; then
        echo "# $2"
    fi
    echo "not ok $TC_NUM - $1"
    TC_NUM="$(( $TC_NUM + 1 ))"
}

# show a TAP-formatted success line
# $1 = TESTCASE name
# optional $2 = extra diagnostic data
succeed() {
    if [ -n "$2" ] ; then
        echo "# $2"
    fi
    echo "ok $TC_NUM - $1"
    TC_NUM="$(( $TC_NUM + 1 ))"
}

v_echo() {
    if [ -n "$VERBOSE" ] ; then
        echo $@
    fi
}

echo "TAP version 13"
echo "1..3"

echo "Using client '$CLIENT' with board $BOARD and device $DEVICE_ID ($DEVICE)"

v_echo "Getting resource and lab endpoint for board"

RESOURCE=$($CLIENT $BOARD get-resource audio $DEVICE_ID)
if [ "$?" != "0" ] ; then
    error_out "Could not get resource for $BOARD:$DEVICE_ID"
fi

echo "Starting audio playback test"
v_echo "RESOURCE=$RESOURCE"

base_filename="$(basename $AUDIO_FILE)"
echo "Uploading audio file $base_filename to board $BOARD"
DUT_TMP_AUDIO_FILE="/tmp/$base_filename"
CAPTURED_AUDIO_FILE="$TMPDIR/captured-audio.wav"

$CLIENT $BOARD ssh upload $AUDIO_FILE $DUT_TMP_AUDIO_FILE

# uses the following variables:
# CLIENT, TESTCASE, BOARD, DEVICE, DEVICE_ID, SAMPLE_RATE
test_one_case() {
    v_echo "==== Doing audio playback actions on $BOARD ===="
    TESTCASE="Audio playback actions with $base_filename"

    #v_echo "Configuring for sample rate $SAMPLE_RATE"

    # NOTE: the receiver is always at a high sample rate
    #echo "{ \"rate\": \"$SAMPLE_RATE\" }" | \
    #    $CLIENT $RESOURCE set-config audio
    #if [ "$?" != "0" ] ; then
    #    fail "$TESTCASE" "Could not set sample rate $SAMPLE_RATE for $RESOURCE"
    #    return
    #fi

    v_echo "Capturing data at lab resource"
    TOKEN="$($CLIENT $RESOURCE audio start)"
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not start audio capture with $RESOURCE"
        return
    fi

    # use for debugging
    #echo "capture token=$TOKEN"

    # give time for receiver to fully settle - I'm not sure this is needed
    sleep 0.3

    # Configure sample rate for the DUT endpoint

    v_echo "Playing audio file from DUT"

    #if [ -z "$SAMPLE_RATE" ] ; then
    #    SAMPLE_RATE=44100
    #fi

    # generate a sample file for playback on the DUT
    $CLIENT $BOARD ssh run "aplay -D $DEVICE $DUT_TMP_AUDIO_FILE"

    v_echo "Stopping audio capture"

    $CLIENT $RESOURCE audio stop $TOKEN
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not stop audio capture"
        return
    fi

    v_echo "Getting captured data into $CAPTURED_AUDIO_FILE"

    # get-data is not good for arbitrary binary data, use get-ref instead,
    # followed by a download
    $CLIENT $RESOURCE audio get-ref $TOKEN -o $CAPTURED_AUDIO_FILE
    if [ "$?" != "0" ] ; then
        fail "$TESTCASE" "Could not get reference to audio data"
        return
    fi

    v_echo "Deleting the data on the server"

    $CLIENT $RESOURCE audio delete $TOKEN || \
        echo "Warning: Could not delete data on server"

    v_echo "AUDIO_FILE -Played- =$AUDIO_FILE"
    v_echo "AUDIO FILE -Captured- =$CAPTURED_AUDIO_FILE"

    succeed "$TESTCASE"

    # Trim the captured file: 1.5 seconds at start and end
    #v_echo "Using SOX to trim 1.5 seconds at start and end of captured file"
    #sox $CAPTURED_AUDIO_FILE $TMPDIR/temp1.wav trim 1.5
    #sox $TMPDIR/temp1.wav $TMPDIR/temp2.wav reverse trim 1.5 reverse

    # Trim the captured file: selience seconds at start and end
    v_echo "Using SOX to silence at start and end of captured file"
    sox $CAPTURED_AUDIO_FILE $TMPDIR/temp1.wav trim 1.0
    sox $TMPDIR/temp1.wav $TMPDIR/temp1a.wav silence 1 0.3 1%
    sox $TMPDIR/temp1a.wav $TMPDIR/temp2.wav reverse silence 1 0.3 1% reverse

    # now actually compare the data to get the testcase result
    # put alsabat or sox comparison in here
    #ALSABAT="./alsabat"
    ALSABAT="/opt/test/bin/alsabat"

    v_echo "==== Doing ALSABAT analysis (tone checking) ===="

    # compare audio at same frequency, and see if it passes the test
    $ALSABAT --readcapture=$TMPDIR/temp2.wav | tee $TMPDIR/audio-test-results.txt

    TESTCASE="Alsabat tone check with $base_filename"

    # look at the results and see what needs to be compared
    if grep PASS $TMPDIR/audio-test-results.txt ; then
        succeed "$TESTCASE"
    else
        fail "$TESTCASE"
    fi

    v_echo "==== Doing SOX analysis ===="

    TESTCASE="SOX frequency check with $base_filename"

    sox $AUDIO_FILE -n stat 2>&1 | tee $TMPDIR/sox-stats-played.txt
    sox $TMPDIR/temp2.wav -n stat 2>&1 | tee $TMPDIR/sox-stats-captured.txt

    PLAYED_FREQ=$(grep frequency $TMPDIR/sox-stats-played.txt | sed s/.*://)
    CAPTURED_FREQ=$(grep frequency $TMPDIR/sox-stats-captured.txt | sed s/.*://)

    echo "PLAYED_FREQ=$PLAYED_FREQ"
    echo "CAPTURED_FREQ=$CAPTURED_FREQ"

    DELTA_THRESHOLD=120
    DELTA=$(( PLAYED_FREQ - CAPTURED_FREQ ))
    DELTA=${DELTA#-}

    echo "Frequency Delta=$DELTA"
    if [ $DELTA -gt $DELTA_THRESHOLD ] ; then
        fail "$TESTCASE"
    else
        succeed "$TESTCASE"
    fi

    if [ -z "$NOREMOVE" ] ; then
        v_echo "Cleaning up"

        v_echo "Removing DUT audio file: $DUT_TMP_AUDIO_FILE"
        $CLIENT $BOARD ssh run "rm $DUT_TMP_AUDIO_FILE"
        v_echo "Removing captured audio file: $CAPTURED_AUDIO_FILE"
        rm $CAPTURED_AUDIO_FILE
        rm $TMPDIR/temp1.wav $TMPDIR/temp1a.wav $TMPDIR/temp2.wav $TMPDIR/audio-test-results.txt
        rm $TMPDIR/sox-stats-played.txt $TMPDIR/sox-stats-captured.txt
    fi
}

test_one_case

echo "Done."
