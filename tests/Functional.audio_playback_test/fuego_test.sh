NO_BOARD_TESTLOG=1
#tarball=

function test_pre_check {
    if [ $TRANSPORT != "lc" -a $TRANSPORT != "ebf" ] ; then
        abort "Transport $TRANSPORT is not supported for board farm tests"
    fi
    assert_has_program aplay
}

function test_build {
    # nothing to build for device
    # FIXTHIS - make my own alsabat program?, for host?
    true
}

function test_run {
    DEVICE=${BF_NAME:-$NODE_NAME}
    pushd $TEST_HOME
    export CLIENT="$TRANSPORT"
    log_this "./audio-playback-test.sh -v --noremove $DEVICE \
        $AUDIO_PLAY_DEVICE $AUDIO_PLAY_ID ./997-tone.wav"
}
