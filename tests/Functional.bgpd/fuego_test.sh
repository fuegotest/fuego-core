function test_pre_check {
    assert_has_program bgpd
    assert_has_program zebra
}

function test_deploy {
    put $TEST_HOME/bgpd_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    cmd "sed -i 's/!interface xxx/interface $IFETH/' $BOARD_TESTDIR/fuego.$TESTDIR/data/zebra.conf"
    cmd "sed -i 's/ip route xxx\/24 xxx reject/ip route $ROUTER\/24 $IFETH reject/' $BOARD_TESTDIR/fuego.$TESTDIR/data/zebra.conf"
    cmd "sed -i 's/bgp router-id xxx/bgp router-id $ROUTER_ID/' $BOARD_TESTDIR/fuego.$TESTDIR/data/bgpd.conf"
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        sh bgpd_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
