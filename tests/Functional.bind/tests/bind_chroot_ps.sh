#!/bin/sh

#  In the target start named chroot, and confirm the process condition by command ps.

test="chroot_ps"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
killall -9 named
exec_service_on_target dnsmasq stop

if [ -d /var/named/ ]
then
    mv /var/named /var/named_bak
fi

mkdir -p /var/named/chroot/etc/bind
cp -f /etc/bind/* /var/named/chroot/etc/bind/
mkdir -p /var/named/chroot/var/named
mkdir -p /var/named/chroot/var/cache/bind
mkdir -p /var/named/chroot/var/run/named

named -t /var/named/chroot

if ps aux | grep "[/]var/named/chroot"
then
    echo " -> get the process of named."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get the process of named."
    echo " -> $test: TEST-FAIL"
fi
killall -9 named
rm -rf /var/named
if [ -d /var/named_bak ]
then
    mv /var/named_bak /var/named
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
