#!/bin/sh

#  In the target start bind, and confirm the process condition by command ps.

test="named_ps"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop
exec_service_on_target named stop

sleep 5

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/named"
then
    echo " -> get the process of named."
else
    echo " -> can't get the process of named."
    echo " -> $test: TEST-FAIL"
    if [ "$named_status" = "inactive" ]
    then
        exec_service_on_target named stop
    fi
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

exec_service_on_target named stop

if ps aux | grep "[/]usr/sbin/named"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
