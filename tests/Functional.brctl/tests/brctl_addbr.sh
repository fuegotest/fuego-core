#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : addbr

test="brctl->addbr"

brctl addbr br0
if ifconfig br0
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
brctl delbr br0
