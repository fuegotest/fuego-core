#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : setageing

test="brctl->setageing"

brctl addbr br0
brctl setageing br0 200.00
if brctl showstp br0 | grep "ageing time.*200.00"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
brctl delbr br0
