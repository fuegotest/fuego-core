#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : sethello

test="brctl->sethello"

brctl addbr br0
brctl sethello br0 5.00
if brctl showstp br0 | grep "hello time.*5.00"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
brctl delbr br0
