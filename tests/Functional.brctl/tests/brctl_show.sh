#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : show

test="brctl->show"

brctl addbr br0
if brctl show | grep br0
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
brctl delbr br0
