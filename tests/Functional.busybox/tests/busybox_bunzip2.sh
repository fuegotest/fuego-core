#!/bin/sh
test="bunzip2"

echo "This is a test file">test1
bzip2 test1
if [ "$(busybox bunzip2 -c test1.bz2)" = "This is a test file" ] ;
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test1.bz2;
