#!/bin/sh

#  The testscript checks the following options of the command chgrp
#  1) Option: -R

test="chgrp2"

mkdir test_dir
touch test_dir/test1
touch test_dir/test2
group=$(id -n -g | cut -b -8)
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f4 | cut -b -8 > log1
if [ "$(head -n 1 log1)" = "$group" ] && [  "$(tail -n 1 log1)" = "$group" ]
then
    echo " -> $test: Group info display succeeded."
else
    echo " -> $test: Group info display failed."
    echo " -> $test: TEST-FAIL"
    rm log1
    rm -rf test_dir
    exit
fi

busybox chgrp -R bin ./test_dir
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f4 | cut -b -8 > log2
if [ "$(head -n 1 log2)" = "bin" ] && [ "$(tail -n 1 log2)" = "bin" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log1 log2
rm -rf test_dir
