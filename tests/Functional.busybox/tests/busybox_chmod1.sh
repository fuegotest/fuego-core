#!/bin/sh

#  The testscript checks the following options of the command chmod
#  1) Option: g+x
#  2) Option: o+x
#  3) Option: a-x

test="chmod1"

mkdir test_dir
touch test_dir/test1

if [ $(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f9) = "test1" ]
then
    echo " -> $test: test_dir contents verification succeeded."
else
    echo " -> $test: test_dir contents verification failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir
    exit
fi;

busybox chmod g+x ./test_dir/test1
if [ "$(busybox ls -l ./test_dir | grep -v "total" | cut -b 5-7)" = "r-x" ]
then
    echo " -> $test: Changed file permissions verification#2 succeeded."
else
    echo " -> $test: Changed file permissions verification#2 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir
    exit
fi;

busybox chmod o+x ./test_dir/test1
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,9  | cut -b 9-16)" = "-x test1" ]
then
    echo " -> $test: Changed file permissions verification#3 succeeded."
else
    echo " -> $test: Changed file permissions verification#3 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir
    exit
fi;

busybox chmod a-x ./test_dir/test1
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,9 | cut -b 1,4,7,10-16 )" = "---- test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf ./test_dir;
