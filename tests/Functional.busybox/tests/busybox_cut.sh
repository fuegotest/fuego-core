#!/bin/sh

#  The testscript checks the following options of the command cut
#  1) Option: -f -d

test="cut"

if [ "$(echo "Hello world" | busybox cut -f 1 -d ' ')" = "Hello" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
