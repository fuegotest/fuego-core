#!/bin/sh

#  The testscript checks the following options of the command env
#  1) Option: none

test="env"

test_dir="$(pwd)"
if [ -f $test_dir/test1_env ]
then
    rm -f $test_dir/test1_env
fi

test_string="TEST_ENV=\$TEST_ENV"
echo \#\!/bin/sh>$test_dir/test1_env
echo "echo $test_string">> $test_dir/test1_env
chmod 777 $test_dir/test1_env
if [ "$($test_dir/test1_env)" = "TEST_ENV=" ]
then
    echo " -> $test: chmod 777 $test_dir/test1_env executed."
else
    echo " -> $test: TEST-FAIL"
    rm -f $test_dir/test1_env
    exit
fi;

if [ "$(busybox env TEST_ENV=1 $test_dir/test1_env)" = "TEST_ENV=1" ]
then
    echo " -> $test: busybox env TEST_ENV=1 $test_dir/test1_env executed."
else
    echo " -> $test: TEST-FAIL"
    rm -f $test_dir/test1_env
    exit
fi;

if [ "$(busybox env TEST_ENV=2 $test_dir/test1_env)" = "TEST_ENV=2" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -f $test_dir/test1_env
