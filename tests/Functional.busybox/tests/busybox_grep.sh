#!/bin/sh

#  The testscript checks the following options of the command grep
#  1) Option: -H -h -i

test="grep"

echo "Fixed String" >test1
echo "In pattern" >>test1
echo "For grep" >>test1

if [ "$(busybox grep -H in ./test1)" = "./test1:Fixed String" ]
then
    echo " -> $test: option -H verification#1 succeeded."
else
    echo " -> $test: option -H verification#1 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test1
    exit
fi;

if [ "$(busybox grep -h in ./test1)" = "Fixed String" ]
then
    echo " -> $test: option -h verification#2 succeeded."
else
    echo " -> $test: option -h verification#2 failed."
    echo " -> $test: TEST-FAIL option -h verfication#2"
    rm -rf test1
    exit
fi;

busybox grep -i in ./test1 > log
if [ "$(head -n 1 log)" = "Fixed String" ] && [ "$(tail -n 1 log)" = "In pattern" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL pattern not found"
fi;
rm log
rm -rf test1
