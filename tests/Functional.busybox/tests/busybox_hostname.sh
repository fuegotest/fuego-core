#!/bin/sh

#  The testscript checks the following options of the command hostname
#  1) Option none

test="hostname"

if [ $(busybox hostname) = $(cat /etc/hostname) ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
