#!/bin/sh

#  The testscript checks the following options of the command pidof
#  1) Option none

test="pidof"

proc1_cmd=$(cat /proc/1/comm)
pid=$(busybox pidof $proc1_cmd)
echo "Process 1 command ($proc1_cmd) has pid(s) $pid"

# 'pid' may have multiples pid strings (systemd does on my system)
# check for 1 by itself, or at start, middle, or end of list
if test "$pid" = 1 || echo $pid | grep "^1 " || echo $pid | grep " 1 " || echo $pid | grep " 1$"
then
    echo " -> $test: get pid from process name succeeded."
else
    echo " -> $test: get pid from process name failed."
    echo " -> $test: TEST-FAIL"
fi;

if busybox pidof test_for_pidof
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi;
