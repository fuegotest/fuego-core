#!/bin/sh

#  The testscript checks the following options of the command pwd
#  1) Option none

test="pwd"

bb_wd=$(busybox pwd)
proc_wd=$(ls -l /proc/$$/cwd)
proc_wd="${proc_wd##*-> }"
echo "bb_wd=$bb_wd"
echo "proc_wd=$proc_wd"
if [ "$bb_wd" = "$proc_wd" ]
then
    echo " -> $test: TEST-PASS"
else
    echo "  pwd from busybox did not match cwd from /proc"
    echo " -> $test: TEST-FAIL"
fi;
