#!/bin/sh

#  The testscript checks the following options of the command readlink
#  1) Option none

test="readlink"

mkdir test_dir1 test_dir2
touch test_dir1/test1
ln -s test_dir1/test1 test_dir2/test2
if [ "$(busybox readlink test_dir2/test2)" = "test_dir1/test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir1 test_dir2;
