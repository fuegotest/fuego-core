#!/bin/sh

#  The testscript checks the following options of the command route

test="route"

busybox route add -net 22.0.0.0 netmask 255.0.0.0 reject

if busybox route | grep 22.0.0.0
then
    echo " -> $test: busybox route add succedded."
else
    echo " -> $test: busybox route add failed."
    echo " -> $test: TEST-FAIL"
fi;

busybox route del -net 22.0.0.0 netmask 255.0.0.0 reject

if ! busybox route | grep 22.0.0.0
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: busybox route del failed."
    echo " -> $test: TEST-FAIL"
fi;
