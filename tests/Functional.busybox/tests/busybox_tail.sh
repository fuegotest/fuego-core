#!/bin/sh

#  The testscript checks the following options of the command tail
#  1) Option: -n

test="tail"

mkdir test_dir
touch test_dir/test1
echo "This is the 1st line." > test_dir/test1
echo "This is the 2nd line." >> test_dir/test1
echo "This is the 3rd line." >> test_dir/test1
busybox tail -n 2 test_dir/test1 > log
if [ "$(head -n 1 log)" = "This is the 2nd line." ] && [ "$(tail -n 1 log)" = "This is the 3rd line." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir;
rm log;
