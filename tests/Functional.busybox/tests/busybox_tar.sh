#!/bin/bh

#  The testscript checks the following options of the command tar
#  1) Option: -cvf -xvf

tar_tst_dir=test/test_dir
test="tar"

if [ -f $tar_tst_dir ]
then
    rm -rf $tar_tst_dir
fi;

mkdir -p $tar_tst_dir
mkdir -p $tar_tst_dir/test_dir1
echo This is a test file to test tar.> $tar_tst_dir/test_dir1/test_tar
busybox tar -cvf $tar_tst_dir/test.tar $tar_tst_dir/test_dir1
if [ "$(ls -l $tar_tst_dir | grep ".*test.tar.*")" ]
then
    echo " -> $test: tar -cvf executed."
else
    echo " -> $test: tar -cvf failed."
    echo " -> $test: TEST-FAIL"
    rm -rf $tar_tst_dir
    exit
fi;

rm -rf $tar_tst_dir/test_dir1
busybox tar -xvf $tar_tst_dir/test.tar -C .
if [ "$(ls -l $tar_tst_dir/test_dir1/test_tar | grep ".*\/test_dir1\/test_tar.*")" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf $tar_tst_dir;
