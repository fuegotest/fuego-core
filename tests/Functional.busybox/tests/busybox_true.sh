#!/bin/sh

#  The testscript checks the following options of the command true
#  1) Option: none

test="true"

if busybox true
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
