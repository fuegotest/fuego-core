#!/bin/sh

#  The testscript checks the following options of the command uniq
#  1) Option none

test="uniq"

echo "a" > test1
echo "a" >> test1
echo "b" >> test1
echo "c" >> test1
echo "c" >> test1
echo "a" >> test1
sort test1 | busybox uniq > log

if [ "$(head -n 1 log)" = "a" ] && [ "$(head -n 2 log | tail -n 1)" = "b" ] && [ "$(tail -n 1 log)" = "c" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm test1;
rm log;
