# Functional.check_mounts
#  This test checks to see if the any of mounted filesystems are
#  different from a a baseline snapshot taken some time in the past.
#
#  The purpose of this test is to do a high-level comparison of the
#  mount points of filesystem, to see if anything has changed since
#  the last baseline snapshot was made.
#
# to do:
#  add possibility of using filters to only test certain filesystems
#    e.g. only "real" filesystems like ext or btrfs, or only
#    filesystems mounted on certain devices (like /dev/sda*)
#     - use FILTER_NAME and FILTER_REGEX
#       - use NAME as part of baseline filename
#
# Usage:
#  Preparation:
#    Create test job:
#      $ ftc add-job -b <board> -t Functional.check_mounts -s default
#    Run the test the first time:
#      $ ftc build-job <board>.default.Functional.check_mounts
#      This test will report failure, but will save the baseline file
#      (list of currently mounted filesystems) in
#         fuego-rw/boards/<board>/Functional.check_mounts-baseline-data.txt
#    Validate the baseline data:
#      Manually inspect the data in the baseline-data file.  If it is correct
#       then remove the warning lines from the top of the file.
#
# Periodic usage:
#    Run the test as part of your test cycle:
#      $ ftc build-job <board>.default.Functional.check_mounts
#
# If the mounts change, or the baseline data is incorrect:
#    Create a 'save_baseline' job
#      $ ftc add-job -b <board> -t Functional.check_mounts -s save_baseline
#    Run the job to save the overwrite the baseline with new data:
#      $ ftc build-job <board>.save_baseline.Functional.check_mounts
#
# To permanently save the data (prevent it from being overridden by
# a save_basline operation), copy the baseline-data file from:
#   fuego-rw to fuego-rw
#      $ cp fuego-rw/boards/<board>/Functional.check_mounts-baseline-data.txt fuego-ro/boards/<board>
#

function test_pre_check {
    export board_rw_dir=$FUEGO_RW/boards/$NODE_NAME
    export board_ro_dir=$FUEGO_RO/boards/$NODE_NAME

    mkdir -p $board_rw_dir

    # check for existence of baseline file
    FILTER_NAME="$FUNCTIONAL_CHECK_MOUNTS_FILTER_NAME"
    FILTER_REGEX="$FUNCTIONAL_CHECK_MOUNTS_FILTER_REGEX"

    # baseline filename includes name of filter (if any), or "default"
    NAME_PART=${FILTER_NAME:-default}

    export rw_baseline_file=$board_rw_dir/$TESTDIR-$NAME_PART-baseline-data.txt
    export ro_baseline_file=$board_ro_dir/$TESTDIR-$NAME_PART-baseline-data.txt

    # but only check if we're not doing the save operation
    if [ "$TESTSPEC" != "save_baseline" ] ; then
        if [ ! -f $rw_baseline_file ] ; then
            if [ -f $ro_baseline_file ] ; then
                cp $ro_baseline_file $rw_baseline_file
            else
                echo "Warning: Missing baseline results file: $rw_baseline_file1"
                echo "Run the test with the 'save_baseline' spec to create the file."
            fi
        fi
    fi
}

function test_run {
    echo "Getting mount points"

    DATA_FILE=$LOGDIR/mount_points.txt

    # get the mounted filesystems from the board
    # could also use 'cat /proc/mounts' here
    if [ -n "$FILTER_NAME" ] ; then
        cmd "mount" | grep $FILTER_REGEX >$DATA_FILE
    else
        cmd "mount" >$DATA_FILE
    fi
    log_this "echo \"### Here are the currently mounted filesystems on $NODE_NAME:\""
    log_this "cat $DATA_FILE"
    log_this "echo --------"

    # if we're doing the "save_baseline" spec, save the data
    if [ "$TESTSPEC" = "save_baseline" ] ; then
        cp $DATA_FILE $rw_baseline_file
        log_this "echo \"baseline file: $baseline_file saved\""
        log_this "echo \"ok 1 compare system mounted points with baseline data\""
        return 0
    fi

    # if baseline data is missing, save the data with a warning
    if [ ! -f $rw_baseline_file ] ; then
        echo "# Warning: baseline data saved automatically." >$rw_baseline_file
        echo "# Please review this data, and remove this header if the data is correct." >>$rw_baseline_file
        cat $DATA_FILE >>$rw_baseline_file
        log_this "echo \"baseline file: $rw_baseline_file saved\""
    fi

    # check for differences from baseline
    set +e
    log_this "echo \"Checking for differences in mount points\""
    log_this "diff -u $rw_baseline_file $DATA_FILE"
    diff_rcode="$?"
    log_this "echo ------------------------"
    set -e

    if [ $diff_rcode == "0" ] ; then
        log_this "echo \"no changes found between current mount points and baseline\""
	    log_this "echo \"ok 1 compare system mounted filesystems with baseline data\""
    else
        log_this "echo \"mounted filesystems are different from baseline\""
	    log_this "echo \"not ok 1 compare system mounted filesystems with baseline data\""
    fi

}

function test_processing {
    log_compare $TESTDIR 1 "^ok " "p"
}

