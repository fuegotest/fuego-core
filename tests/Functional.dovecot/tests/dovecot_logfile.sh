#!/bin/sh

#  In the target to start dovecot dovecot, to confirm the acquisition of the log.
#  check the keyword "dovecot".

test="logfile"

service_status=$(get_service_status dovecot)
logger_service=$(detect_logger_service)

exec_service_on_target dovecot stop

if [ -f /var/log/mail.log ]
then
    mv /var/log/mail.log /var/log/mail.log_bak
fi

if [ -f /var/log/messages ]
then
    mv /var/log/messages /var/log/messages_bak
fi

restore_target() {
    if [ -f /var/log/messages_bak ]
    then
        mv /var/log/messages_bak /var/log/messages
    fi
    if [ -f /var/log/mail.log_bak ]
    then
        mv /var/log/mail.log_bak /var/log/mail.log
    fi
}

exec_service_on_target $logger_service restart

sleep 3

if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 3

if tail /var/log/mail.log | grep "dovecot"
then
    echo " -> grep dovecto from mail.log"
else
    echo " -> can't grep dovecto from mail.log"
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target dovecot stop
restore_target
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target dovecot start
fi
