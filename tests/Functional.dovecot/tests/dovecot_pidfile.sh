#!/bin/sh

#  In the target to start dovecot dovecot, to confirm the acquisition of the log.
#  check the file master.pid.

test="pidfile"

service_status=$(get_service_status dovecot)

exec_service_on_target dovecot stop

if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
fi

sleep 5

if ls /var/run/dovecot/master.pid
then
    echo " -> ls /var/run/dovecot/master.pid succeeded."
else
    echo " -> ls /var/run/dovecot/master.pid failed."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target dovecot stop
    fi
    exit
fi

exec_service_on_target dovecot stop

if test ! -f /var/run/dovecot/master.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target dovecot start
fi
