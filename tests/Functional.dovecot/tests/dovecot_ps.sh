#!/bin/sh

#  In the target start dovecot, and confirm the process condition by command ps.
#  check the keyword "dovecot".

test="ps"

service_status=$(get_service_status dovecot)

exec_service_on_target dovecot stop
if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep "[/]usr/sbin/dovecot"
then
    echo " -> get the pid of dovecot."
else
    echo " -> can't get the pid of dovecot."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target dovecot stop
    fi
    exit
fi

if exec_service_on_target dovecot stop
then
    echo " -> stop of dovecot succeeded."
else
    echo " -> stop of dovecot failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep "[/]usr/sbin/dovecot"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target dovecot start
fi
