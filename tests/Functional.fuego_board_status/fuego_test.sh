# This test saves off a bunch of board information, for use
# as a baseline, in case the board dosn't 
function test_deploy {
    put $TEST_HOME/board_status.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./board_status.sh $BOARD_TESTDIR"
}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"
}
