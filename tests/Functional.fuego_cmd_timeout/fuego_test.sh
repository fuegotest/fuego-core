# this function sleeps for the time specified by the
# spec (default=300 seconds, or 5 minutes)

function test_run {
    TIMEOUT=10
    SLEEP_TIME=20

    TIMEOUT2=20
    SLEEP_TIME2=5

    TIMEOUT3=0
    SLEEP_TIME3=10

    report "echo TAP version 13"
    report_append "echo Before timeout of sleep operation"

    ##### test with timeout, that times out

    report "echo Testing timeout using sleep of $SLEEP_TIME seconds and timeout of ${TIMEOUT}s"

    set +e
    export FUEGO_CMD_TIMEOUT="${TIMEOUT}s"
    report_append "sleep $SLEEP_TIME"
    SAVED_RC=$?
    set -e
    SAVED_DURATION=$FUEGO_CMD_DURATION
    SAVED_TIMED_OUT=$FUEGO_CMD_TIMED_OUT
    FUEGO_CMD_TIMEOUT=""
    report_append "echo return code=$SAVED_RC"
    report_append "echo FUEGO_CMD_DURATION=$SAVED_DURATION"
    report_append "echo FUEGO_CMD_TIMED_OUT=$SAVED_TIMED_OUT"

    # check that cmd timed out
    if [ -n "$SAVED_TIMED_OUT" ] ; then
        report_append "echo ok 1 - Long command timed out"
    else
        report_append "echo not ok 1 - Long command timed out"
    fi

    # check that timed out duration was accurate
    if [ "$SAVED_DURATION" -ge "$TIMEOUT" -a "$SAVED_DURATION" -le $(( TIMEOUT + 5 )) ] ; then
        report_append "echo ok 2 - Command timeout time is close enough"
    else
        report_append "echo not ok 2 - Command timeout time is close enough"
    fi

    ##### test with timeout, that doesn't time out

    report_append "echo Testing timeout using sleep of $SLEEP_TIME2 seconds and timeout of ${TIMEOUT2}s"
    set +e
    FUEGO_CMD_TIMEOUT="${TIMEOUT2}s"
    report_append "sleep $SLEEP_TIME2"
    SAVED_RC=$?
    set -e
    SAVED_DURATION=$FUEGO_CMD_DURATION
    SAVED_TIMED_OUT=$FUEGO_CMD_TIMED_OUT
    FUEGO_CMD_TIMEOUT=""
    report_append "echo return code=$SAVED_RC"
    report_append "echo FUEGO_CMD_DURATION=$SAVED_DURATION"
    report_append "echo FUEGO_CMD_TIMED_OUT=$SAVED_TIMED_OUT"

    # check that cmd did not time out
    if [ -z "$SAVED_TIMED_OUT" ] ; then
        report_append "echo ok 3 - Short command does not time out"
    else
        report_append "echo not ok 3 - Short command does not time out"
    fi

    # check that cmd duration was accurate
    if [ "$SAVED_DURATION" -ge "$SLEEP_TIME2" -a "$SAVED_DURATION" -le $(( SLEEP_TIME2 + 5)) ] ; then
        report_append "echo ok 4 - Command duration is close enough"
    else
        report_append "echo not ok 4 - Command duration is close enough"
    fi

    ##### test with no timeout

    report_append "echo Testing timeout using sleep of $SLEEP_TIME3 seconds and no timeout"

    FUEGO_CMD_TIMEOUT=""
    report_append "sleep $SLEEP_TIME3"
    SAVED_RC=$?
    SAVED_DURATION=$FUEGO_CMD_DURATION
    SAVED_TIMED_OUT=$FUEGO_CMD_TIMED_OUT
    FUEGO_CMD_TIMEOUT=""
    report_append "echo return code=$SAVED_RC"
    report_append "echo FUEGO_CMD_DURATION=$SAVED_DURATION"
    report_append "echo FUEGO_CMD_TIMED_OUT=$SAVED_TIMED_OUT"

    # check that cmd did not time out
    if [ -z "$SAVED_TIMED_OUT" ] ; then
        report_append "echo ok 5 - Command with no timeout does not time out"
    else
        report_append "echo not ok 5 - Command with no timeout does not time out"
    fi

    # check that there is no duration measurement if no timeout is specified
    if [ -z "$SAVED_DURATION" ] ; then
        report_append "echo ok 6 - Command with no timeout has no duration"
    else
        report_append "echo not ok 6 - Command with no timeout has no duration"
    fi
}

#function test_processing {
#    log_compare "$TESTDIR" "6" "^ok" "p"
#}
