
function test_run {
    log_this "echo Starting dependency cache checks"

    # some commands may fail
    set +e

    # now do some host-driven direct tests of assert_has_program

    # first, clear the program cache
    ftc delete-var -b $NODE_NAME PROGRAM_LS
    ftc delete-var -b $NODE_NAME PROGRAM_LL
    unset CACHE_DEPENDENCIES

    desc="check for ls using old system"
    start_time=$(date +"%s.%N")
    assert_has_program_slow ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")

    log_this "echo \"# after assert_has_program PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 1 - $desc"
    else
        log_this "echo not ok 1 - $desc"
    fi

    unset PROGRAM_LS
    log_this "echo \"# after unset, PROGRAM_LS=$PROGRAM_LS\""

    desc="check for ls using new system"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")
    log_this "echo \"# PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 2 - $desc"
    else
        log_this "echo not ok 2 - $desc"
    fi

    desc="check if 'ls' is in the cache, expecting no"
    cache_var=$(ftc query-board -b $NODE_NAME -n PROGRAM_LS 2>/dev/null)
    log_this "echo \"# cache_var PROGRAM_LS=$cache_var\""
    if [ "$cache_var" = "" ] ; then
        log_this "echo ok 3 - $desc"
    else
        log_this "echo not ok 3 - $desc"
    fi

    unset PROGRAM_LS
    log_this "echo \"# after unset, PROGRAM_LS=$PROGRAM_LS\""

    export CACHE_DEPENDENCIES=1
    desc="check for ls using new system, with caching"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")
    log_this "echo \"# CACHE_DEPENDENCIES=$CACHE_DEPENDENCIES\""
    log_this "echo \"# after assert_has_program PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 4 - $desc"
    else
        log_this "echo not ok 4 - $desc"
    fi

    desc="check if 'ls' is in the cache, expecting yes"
    cache_var=$(ftc query-board -b $NODE_NAME -n PROGRAM_LS 2>/dev/null)
    log_this "echo \"# cache_var PROGRAM_LS=$cache_var\""
    if [ "$cache_var" = "/bin/ls" ] ; then
        log_this "echo ok 5 - $desc"
    else
        log_this "echo not ok 5 - $desc"
    fi

    desc="check for ls using new system, with data in cache"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")
    log_this "echo \"# PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 6 - $desc"
    else
        log_this "echo not ok 6 - $desc"
    fi

    set -x
    cmd "command -v ll"
    cmd "command -v alias"
    set +x

    desc="check for ll alias using new system"
    start_time=$(date +"%s.%N")
    check_has_program ll
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")
    log_this "echo \"# PROGRAM_LL=$PROGRAM_LL\""
    log_this "echo \"# check duration=$duration\""
    # FIXTHIS - this will always fail, unless ll is defined in DUT user
    # account's .profile or .bashrc
    # aliases don't persist over time, so I can't set one and then run check_has_program
    # and a missing alias will produce PROGRAM_LL=<missing>, not an empty string

    # this is a shoddy test, for the following reasons:
    # 1) there's no control to check that DUT has an 'll' alias
    # presumably this test was written to see if check_has_program can 'drill through'
    # an alias and find out that the desired program (ll, not ls) does not exist.
    # But this would only work if the test were
    # run on a system that already had ll defined as an alias, and it did not
    # have a 'real' ll program on the system. (which are both bad assumptions)
    if [ "$PROGRAM_LL" = "<missing>" ] ; then
        log_this "echo ok 7 - $desc"
    else
        log_this "echo not ok 7 - $desc"
    fi

    # FIXTHIS - egrep might not be an alias (in fact, it's probably not on most DUTs)
    # this is a shoddy test, for the following reasons:
    # 1) there's no control to check that DUT has an 'egrep' alias
    # 2) a DUT might not have 'egrep' at all
    # 3) the egrep binary can be in /usr/bin/egrep, not /bin/egrep
    # presumably this test was written to see if check_has_program can 'drill through'
    # an alias and find the program underneath.  But this only works if the test is
    # run on a system that already has egrep defined as an alias, and the actual
    # egrep binary or script is in /bin/egrep (which are both bad assumptions)
    desc="check for egrep alias using new system"
    start_time=$(date +"%s.%N")
    check_has_program egrep
    end_time=$(date +"%s.%N")
    duration=$(python -c "print($end_time - $start_time)")
    log_this "echo \"# PROGRAM_EGREP=$PROGRAM_EGREP\""
    log_this "echo \"# check duration=$duration\""
    if [ "$PROGRAM_EGREP" = "/bin/egrep" -o "$PROGRAM_EGREP" = "/usr/bin/egrep" ] ; then
        log_this "echo ok 8 - $desc"
    else
        log_this "echo not ok 8 - $desc"
    fi
}
