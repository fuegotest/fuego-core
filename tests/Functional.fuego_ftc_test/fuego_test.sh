function test_run {
    if [ -f $FUEGO_RW/boards/ftc-test.vars ] ; then
        rm $FUEGO_RW/boards/ftc-test.vars
    fi
    log_this "clitest --prefix tab $TEST_HOME/ftc-tests"
    if [ -f $FUEGO_RW/boards/ftc-test.vars ] ; then
        rm $FUEGO_RW/boards/ftc-test.vars
    fi
}

function test_processing {
    # log_compare "$TESTDIR" "1" "^OK: 30 of 30" "p"
    log_compare "$TESTDIR" "0" "^[[]FAILED " "0"
}
