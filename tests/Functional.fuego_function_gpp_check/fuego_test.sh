function test_run {
    log_this "echo TAP version 13"
    log_this "echo 1..7"

    # don't stop on test errors
    set +e
    # try checking the docker container for status of things
    testcase_num=1
    desc="find a program on the path on the target"

    get_program_path ls
    log_this "echo PROGRAM_LS=$PROGRAM_LS"
    if [ "${PROGRAM_LS}" = "/bin/ls" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_LS

    desc="attempt missing command, with default default"
    get_program_path fooprog
    echo PROGRAM_FOOPROG=$PROGRAM_FOOPROG

    EXPECTED_DEFAULT=$BOARD_TESTDIR/fuego.$TESTDIR/fooprog
    if [ "${PROGRAM_FOOPROG}" = "$EXPECTED_DEFAULT" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_FOOPROG

    desc="attempt missing command, with specified default"
    get_program_path fooprog /bin /opt/foo/bin/fooprog
    echo PROGRAM_FOOPROG=$PROGRAM_FOOPROG
    EXPECTED_DEFAULT="/opt/foo/bin/fooprog"
    if [ "${PROGRAM_FOOPROG}" = "$EXPECTED_DEFAULT" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_FOOPROG

    # do this in /tmp instead of /opt, because some
    # test users don't have permissions to modify /opt
    desc="find program with additional location"
    cmd "mkdir -p /tmp/foo/bin ; echo \"\#\!/bin/sh\" > /tmp/foo/bin/fooprog ; chmod a+x /tmp/foo/bin/fooprog"
    get_program_path fooprog /tmp/foo/bin
    echo PROGRAM_FOOPROG=$PROGRAM_FOOPROG
    EXPECTED_DEFAULT="/tmp/foo/bin/fooprog"
    if [ "${PROGRAM_FOOPROG}" = "$EXPECTED_DEFAULT" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    cmd "rm /tmp/foo/bin/fooprog ; rmdir /tmp/foo/bin ; rmdir /tmp/foo"
    unset PROGRAM_FOOPROG

    desc="attempt missing program, with additional location, and default default"
    get_program_path fooprog /opt/foo/bin
    echo PROGRAM_FOOPROG=$PROGRAM_FOOPROG
    EXPECTED_DEFAULT=$BOARD_TESTDIR/fuego.$TESTDIR/fooprog
    if [ "${PROGRAM_FOOPROG}" = "$EXPECTED_DEFAULT" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_FOOPROG

    desc="execute program using path from get_program_path"

    get_program_path date
    log_this "echo PROGRAM_DATE=$PROGRAM_DATE"
    if cmd "$PROGRAM_DATE" ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_DATE

    desc="execute builtin using path from get_program_path"
    get_program_path true
    log_this "echo PROGRAM_TRUE=$PROGRAM_TRUE"
    if cmd "$PROGRAM_TRUE" ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi
    testcase_num=$(( testcase_num + 1 ))
    unset PROGRAM_TRUE

    set -e
}
