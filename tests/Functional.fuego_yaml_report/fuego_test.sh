NO_BOARD_TESTLOG="True"

function test_run {
    log_this "echo == Report on Fuego test.yaml files =="

    # NOTE - this doesn't check binary packages, but usually a
    # test.yaml is required in order to create a binary package

    tests=$(ftc -q list-tests)
    found_count=0
    missing_count=0
    for test in $tests ; do
        if [ -f $FUEGO_CORE/tests/$test/test.yaml ] ; then
            found_count=$(( found_count + 1 ))
        else
            missing_count=$(( missing_count + 1 ))
            log_this "echo Missing test.yaml file for test $test"
        fi
    done

    test_count=$(echo $tests | wc -w)
    log_this "echo --------------------"
    log_this "echo Number of test.yaml files found  : $found_count"
    log_this "echo Number of test.yaml files missing: $missing_count"
    log_this "echo Number of total tests            : $test_count"
}

function test_processing {
    log_compare $TESTDIR 0 "Missing" "n"
}
