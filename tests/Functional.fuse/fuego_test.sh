tarball=fuse-2.9.4.tar.gz
LIB_FUSE=""
NEED_ROOT=1

function test_build {
    echo "fuse test build"
    patch -p1 -N < $TEST_HOME/patch_files/0001-fuse-fix-the-return-value-of-help-option.patch
    patch -p1 -N < $TEST_HOME/patch_files/aarch64.patch
    patch -p1 -N < $TEST_HOME/patch_files/gold-unversioned-symbol.patch
    ./configure --host=$PREFIX
    make
    cp util/fusermount  example/.libs
    tar cjf fuse_test_libs.tar.bz2 example/.libs
}

function test_deploy {
    put fuse_test_libs.tar.bz2 $BOARD_TESTDIR/fuego.$TESTDIR/;
    put $TEST_HOME/fuse_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/;

    is_on_target libfuse.so.2 LIB_FUSE /usr/lib
    if [ -z $LIB_FUSE ]; then
        put lib/.libs/libfuse.so.2.9.4 /usr/lib/
        cmd "ln -sf /usr/lib/libfuse.so.2.9.4 /usr/lib/libfuse.so.2"
    fi
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v fuse_test.sh"
    if [ -z $LIB_FUSE ]; then
        cmd "rm /usr/lib/libfuse.so.2.9.4 ; rm /usr/lib/libfuse.so.2"
    fi
}
