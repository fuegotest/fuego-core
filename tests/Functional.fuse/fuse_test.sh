#!/bin/sh

printf "TAP version 13\n1..7"

test_path=$(pwd)
rm -rf $test_path/fuse/
mkdir -p $test_path/fuse/test_hello && \
    tar -jxvf fuse_test_libs.tar.bz2 && \
    cd example/.libs || echo "Bail out! - ERROR no fuse_test_libs.tar.bz2 found."

desc="write to hello filesystem"
if ./hello $test_path/fuse/test_hello ; then
    echo "ok 1 - $desc"; else echo "not ok 1 - $desc";
fi;

desc="read from hello file"
if cat $test_path/fuse/test_hello/hello | grep "Hello World!"; then
    echo "ok 2 - $desc"; else echo "not ok 2 - $desc";
fi;

desc="unmount hello filesystem"
if ./fusermount -u $test_path/fuse/test_hello; then
    echo "ok 3 - $desc"; else echo "not ok 3 - $desc";
fi;

desc="mount fioc filesystem"
mkdir -p $test_path/fuse/test_fioc
if ./fioc -o allow_other -o sync_read -o nonempty \
    -o intr -o big_writes -o remember=1 -o kernel_cache \
    -o kernel_cache $test_path/fuse/test_fioc; then
    echo "ok 4 - $desc"; else echo "not ok 4 - $desc";
fi;
sleep 2
sync

desc="unmount fioc filesystem"
if ./fusermount -u $test_path/fuse/test_fioc/; then
    desc="unmount fioc filesystem"
    echo "ok 5 - $desc"; else echo "not ok 5 - $desc";
fi;

desc="mount fsel filesystem"
mkdir -p $test_path/fuse/test_fsel
if ./fsel -o allow_other -o sync_read -o nonempty \
    -o intr -o big_writes -o remember=1 -o kernel_cache \
    -o kernel_cache $test_path/fuse/test_fsel; then
    echo "ok 6 - $desc"; else echo "not ok 6 - $desc";
fi;
sleep 2
sync

desc="unmount fioc filesystem"
if ./fusermount -u $test_path/fuse/test_fsel/; then
    echo "ok 7 - $desc"; else echo "not ok 7 - $desc";
fi;

# disable the fusexmp test, until it is verified that
# the unmount works.  Otherwise Fuego normal cleanup
# (consisting of rm -rf of $BOARD_TESTDIR) can wipe out
# the root filesystem of a board.
# TRB - 2018-07-31

#mkdir -p $test_path/fuse/test_fusexmp
#if ./fusexmp -o allow_other -o sync_read -o nonempty \
#    -o max_read=16 -o default_permissions \
#    $test_path/fuse/test_fusexmp; then
#    desc="mount fusemp filesystem"
#    echo "ok 8 - $desc"; else echo "not ok 8 - $desc";
#fi;
#if ./fusermount -u $test_path/fuse/test_fusexmp/; then
#    desc="unmount fusemp filesystem"
#    echo "ok 9 - $desc"; else echo "not ok 9 - $desc";
#fi;

# there used to be an 'rm -rf $test_path/fuse' here, but due
# to the use of fusexmp and a failure to unmount it (TEST-9),
# the test wiped out 2 of my boards' root filesystems.  Ugh.
