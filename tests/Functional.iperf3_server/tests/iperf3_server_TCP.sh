#!/bin/sh

#  Test command iperf3 on target.
#  Option : server(TCP)

test="TCP"

killall -KILL iperf3
iperf3 -s -D&

ls .
netstat -l > iperf-test

if cat iperf-test | grep "5201"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

killall -KILL iperf3
rm -f iperf-test
