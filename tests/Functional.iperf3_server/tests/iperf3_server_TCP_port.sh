#!/bin/sh

# Test command iperf3 on target.
# Option : server( TCP port 6000 )

test="TCP port"

killall -KILL iperf3

iperf3 -s -p 6000 >iperf.log &

sleep 3

killall iperf3

if grep "Server listening on 6000" iperf.log
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm iperf.log
