#!/bin/sh

#  Test command iperf3 on target.
#  Option : -c(port opt)

test="client_port"

killall -KILL iperf3
iperf3 -s -D -p 6000&

sleep 3

iperf3 -c 127.0.0.1 -p 6000 >iperf.log &

sleep 3

killall iperf3
if grep "connected" iperf.log
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm iperf.log
