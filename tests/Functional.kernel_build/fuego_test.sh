FUNCTIONAL_KERNEL_BUILD_PER_JOB_BUILD="true"

function test_pre_check {
    echo "Doing a pre_check"

    assert_define ARCHITECTURE
    if [ "$ARCHITECTURE" = "x86_64" ]; then
        is_on_sdk libelf.a LIBELF /usr/lib/$ARCHITECTURE-linux-*/
        assert_define LIBELF
        is_on_sdk libssl.a LIBSSL /usr/lib/$ARCHITECTURE-linux-*/
        assert_define LIBSSL
        is_on_sdk bison PROGRAM_BISON /usr/bin/
        assert_define PROGRAM_BISON
        is_on_sdk flex PROGRAM_FLEX /usr/bin/
        assert_define PROGRAM_FLEX
    fi

    if [ -z "$FUNCTIONAL_KERNEL_BUILD_TARGET" ]; then
        FUNCTIONAL_KERNEL_BUILD_TARGET="bzImage"
    fi

    if [[ "$FUNCTIONAL_KERNEL_BUILD_TARGET" == *"uImage"* ]]; then
        is_on_sdk mkimage
    fi

    # Force the kernel to be rebuilt
    # That's the whole point of the test - don't let Fuego skip the build.
    export Rebuild=True
}

function test_build {
    # make sure we have the latest source code.
    cd "$WORKSPACE/$JOB_BUILD_DIR"
    if [ -d ".git" ]; then
        git pull || echo "WARNING: git pull failed"
    else
        echo "WARNING: no git repository, assuming you used a tarball"
    fi

    # Config
    if [ -z "$FUNCTIONAL_KERNEL_BUILD_CONFIG" ]; then
        FUNCTIONAL_KERNEL_BUILD_CONFIG="defconfig"
    fi

    echo "Configuring kernel with $FUNCTIONAL_KERNEL_BUILD_CONFIG"
    if [ -f "$FUNCTIONAL_KERNEL_BUILD_CONFIG" ]; then
        cp $FUNCTIONAL_KERNEL_BUILD_CONFIG .config
        make ARCH=$ARCHITECTURE olddefconfig
    else
        make ARCH=$ARCHITECTURE $FUNCTIONAL_KERNEL_BUILD_CONFIG
    fi

    # Building
    echo "Building Kernel"
    if [ -z "$FUNCTIONAL_KERNEL_BUILD_PARAMS" ]; then
        FUNCTIONAL_KERNEL_BUILD_PARAMS="-j$(nproc)"
    fi

    log_this "make ARCH=$ARCHITECTURE $FUNCTIONAL_KERNEL_BUILD_PARAMS $FUNCTIONAL_KERNEL_BUILD_TARGET"

    if grep "CONFIG_MODULES is not set" .config; then
        echo "INFO: config does not have modules enabled"
    else
        echo "Packing modules"
        mkdir built_modules
        log_this "make ARCH=$ARCHITECTURE $FUNCTIONAL_KERNEL_BUILD_PARAMS modules"
        log_this "make ARCH=$ARCHITECTURE INSTALL_MOD_STRIP=1 INSTALL_MOD_PATH=./built_modules modules_install"
        cd built_modules
        tar -cjf modules.tar.gz lib/modules/*/
        cd ..
        rm -rf built_modules
    fi
    BUILD_EXECUTED=1
}

function test_deploy {
    # FIXTHIS: add support for device trees
    DEPLOY_METHOD="${FUNCTIONAL_KERNEL_BUILD_DEPLOY_METHOD:-log_dir}"

    if [ "$DEPLOY_METHOD" == "log_dir" ]; then
        echo "Deploying kernel to $LOGDIR"
        cp $WORKSPACE/$JOB_BUILD_DIR/arch/$ARCHITECTURE/boot/$FUNCTIONAL_KERNEL_BUILD_TARGET $LOGDIR
        if [ -f $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz ]; then
            cp $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz $LOGDIR
        fi
    fi

    if [ "$DEPLOY_METHOD" == "local_dir" ]; then
        DEPLOY_PATH="${FUNCTIONAL_KERNEL_BUILD_DEPLOY_PATH:-/var/lib/tftpboot}"
        echo "Deploying kernel to ${DEPLOY_PATH}"
        if [ ! -d $DEPLOY_PATH ]; then
            echo "Deploy path does not exist, trying to make it"
            mkdir -p $DEPLOY_PATH
        fi
        cp $WORKSPACE/$JOB_BUILD_DIR/arch/$ARCHITECTURE/boot/$FUNCTIONAL_KERNEL_BUILD_TARGET "${DEPLOY_PATH}"
        if [ -f $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz ]; then
            cp $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz $DEPLOY_PATH
        fi
    fi

    if [ "$DEPLOY_METHOD" == "board_dir" ]; then
        echo "Deploying kernel to board $NODE_NAME"
        DEPLOY_PATH="${FUNCTIONAL_KERNEL_BUILD_DEPLOY_PATH:-/boot}"
        put $WORKSPACE/$JOB_BUILD_DIR/arch/$ARCHITECTURE/boot/$FUNCTIONAL_KERNEL_BUILD_TARGET "${DEPLOY_PATH}"
        if [ -f $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz ]; then
            put $WORKSPACE/$JOB_BUILD_DIR/modules.tar.gz /
            cmd "cd /; tar xvf modules.tar.gz"
        fi
        cmd "update-grub" || true
    fi
}

function test_processing {
    if [ -z "$BUILD_EXECUTED" ]; then
        echo "test_build was not executed, so there is no log to parse"
        RETURN_VALUE=0
        return $RETURN_VALUE
    else
        echo "Processing kernel build log"
        if [ -z "$FUNCTIONAL_KERNEL_BUILD_REGEX_P" ]; then
            log_compare "$TESTDIR" "1" "[ \t]*Kernel: arch/.* is ready" "p"
        else
            log_compare "$TESTDIR" "1" "$FUNCTIONAL_KERNEL_BUILD_REGEX_P" "p"
        fi
    fi
}

