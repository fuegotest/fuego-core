function test_build {
    mkdir -p kbuild
    export KBUILD_OUTPUT=$(realpath kbuild)

    # this test is a compilation test.  This build phase
    # does the one-time repo download for the test.

    # use current kernel config for board
    kconf_filename=$LOGDIR/kconfig
    if [ ! -f $kconf_filename ] ; then
        get_full_kconfig $LOGDIR
    fi

    if [ -f $kconf_filename ]  ; then
        cp $kconf_filename $KBUILD_OUTPUT/.config
    else
        make ARCH=$ARCHITECTURE defconfig
    fi

    make ARCH=$ARCHITECTURE headers_install
    make ARCH=$ARCHITECTURE vmlinux

    # override kselftest lib.mk override of CC
    # we have some CC definitions in Fuego that are complex
    perl -p -i -e "s/^CC :=/#CC :=/" tools/testing/selftests/lib.mk
}

function test_run {
    INDENT=3
    mkdir -p kbuild
    export KBUILD_OUTPUT=$(realpath kbuild)

    #TARGET_LIST='size timens timers'
    #pushd tools/testing/selftests
    #TARGET_LIST=$(make show_targets)
    #popd
    TARGET_LIST="android arm64 bpf breakpoints capabilities cgroup \
        clone3 cpufreq cpu-hotplug drivers/dma-buf efivarfs exec \
        filesystems filesystems/binderfs filesystems/epoll firmware \
        ftrace futex gpio intel_pstate ipc ir kcmp kexec kvm lib \
        livepatch lkdtm membarrier memfd memory-hotplug mount mqueue \
        net net/mptcp netfilter networking/timestamping nsfs pidfd \
        powerpc proc pstore ptrace openat2 rseq rtc seccomp sigaltstack \
        size sparc64 splice static_keys sync sysctl timens timers tmpfs \
        tpm2 user vm x86 zram"

    # output the header
    log_this "echo \"TAP VERSION 13\""
    target_count=$(echo $TARGET_LIST | wc -w)
    export test_count=$(( target_count * 2 ))
    log_this "echo \"1..$test_count\""

    # disable 'stop-on-error'
    set -e

    test_num=1
    for target in $TARGET_LIST ; do
        test_id="compile $target"
        log_this "echo \"# $test_num $test_id\""

        log_this "make ARCH=$ARCHITECTURE TARGETS=\"$target\" -C tools/testing/selftests | pr -to $INDENT"
        if [ $? == 0 ] ; then
            log_this "echo \"ok $test_num $test_id\""
        else
            log_this "echo \"not ok $test_num $test_id\""
        fi
        test_num=$(( $test_num + 1))

        test_id="install $target"
        log_this "echo \"# $test_num $test_id\""

        log_this "make ARCH=$ARCHITECTURE TARGETS=\"$target\" -C tools/testing/selftests install | pr -to $INDENT"
        install_result=$?

        # FIXTHIS - check result of install operation
        # materials should be in KBUILD_OUTPUT/kselftest/kselftest_install
        log_this "echo \"# Contents of kselftest_install directory:\""
        pushd $KBUILD_OUTPUT/kselftest/kselftest_install >/dev/null
        log_this "ls -lR | pr -to $INDENT"
        log_this "echo \"# File types:\""
        log_this "find . -type f | xargs file -n | sed \"s/BuildID.*, //\" | pr -to $INDENT"

        # FIXTHIS - check that run_install has the target
        popd >/dev/null

        # FIXTHIS - check that any settings files were copied

        if [ $install_result == 0 ] ; then
            log_this "echo \"ok $test_num $test_id\""
        else
            log_this "echo \"not ok $test_num $test_id\""
        fi
        test_num=$(( $test_num + 1))

        # clear out kbuild directory for next test
        rm -rf $KBUILD_OUTPUT/kselftest/kselftest_install
        mkdir -p $KBUILD_OUTPUT/kselftest/kselftest_install
    done
    set +e
}

function test_processing {
    log_compare "$TESTDIR" "$test_count" "^ok" "p"
}
