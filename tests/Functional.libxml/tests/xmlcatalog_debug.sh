#!/bin/sh

#  In the target to execute command xmlcatalog and confirm the result.
#  option : --create,--verbose

test="xmlcatalog3"

xmlcatalog --create > catalog
xmlcatalog --verbose catalog > log 2>&1
if cat log | grep "Catalogs cleanup"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm -f catalog
rm log
