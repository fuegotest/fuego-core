#!/bin/sh

#  In the target to execute command xmllint and confirm the result.
#  option : none

test="xmllint1"

cat >> sample01.xml <<EOF
<?xml version="1.0" encoding="Shift_JIS" ?>

<!DOCTYPE linux[
<!ELEMENT linux (distribution)>
<!ELEMENT distribution (debian,redhat)>
<!ELEMENT debian (#PCDATA)>
<!ELEMENT redhat (#PCDATA)>
]>

<linux>
  <distribution>
    <debian>favorit</debian>
    <redhat>interest</redhat>
  </distribution>
</linux>
EOF

if xmllint sample01.xml
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm sample01.xml
