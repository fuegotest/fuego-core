#!/bin/sh

#  In the target to execute command xmllint and confirm the result.
#  option : --format

test="xmllint4"

cat >> format.xml <<EOF
<root><parent><child><subchild>DOCUMENT</subchild></child><child>
<subchild>TEXT</subchild></child></parent></root>
EOF

if xmllint --format format.xml | wc -l | grep 11
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm format.xml
