#!/bin/sh
# set the log status to one day ago
#

if [ -z "${STATUS_FILE}" ] ; then
    STATUS_FILE=/var/lib/logrotate/status
fi

today=$(date +%s)
# there are 86400 seconds in a day
yesterday=$(( $today - 86400 ))
ddch=$(date --date "@$yesterday" "+%Y-%m-%d")

# make an entry for one day ago, for testlog, in the status file
echo "logrotate state -- version 2" >> ${STATUS_FILE}
echo "\"/var/log/testlog\" $ddch" >> ${STATUS_FILE}

# uncomment these to double-check the contents of the status file
#echo "== logrotate status is:"
#cat ${STATUS_FILE}
