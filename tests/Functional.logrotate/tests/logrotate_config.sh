#!/bin/sh
# check that logrotate parses test config OK
#
#  Turns on verbose mode and confirm the result.
#  option : -dv

test="test config"

if [ -z "${STATUS_FILE}" ] ; then
    STATUS_FILE=/var/lib/rotatelog/status
fi

# setup - save status and install test configuration
if [ -f ${STATUS_FILE} ] ; then
    mv ${STATUS_FILE} ${STATUS_FILE}_bak
fi

cp data/test.conf /etc/logrotate.d/test.conf
chown root /etc/logrotate.d/test.conf

# see if logrotate likes the test configuration
if logrotate -dv /etc/logrotate.d/test.conf ; then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

# cleanup
if [ -f ${STATUS_FILE}_bak ] ; then
    mv ${STATUS_FILE}_bak ${STATUS_FILE}
else
    rm ${STATUS_FILE}
fi
rm -f /etc/logrotate.d/test.conf
