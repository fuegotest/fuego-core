#!/bin/sh
#  Verify that the rotate file is created in the specified directory.
#  option : -v

test="log rotation"

if [ -z "${STATUS_FILE}" ] ; then
    STATUS_FILE=/var/lib/logrotate/status
fi

# setup - remove old logs, install config, set status
if [ -f /var/log/testlog* ] ; then
    rm -f /var/log/testlog*
fi

if [ -f ${STATUS_FILE} ] ; then
    mv ${STATUS_FILE} ${STATUS_FILE}_bak
fi

# put config, existing log, and status-writing script into system
cp data/test.conf /etc/logrotate.d/test.conf
chown root /etc/logrotate.d/test.conf

cp data/testlog100k /var/log/testlog

# add an entry to the status indicating the last time it was rotated
cp data/testlog.sh data/set_logstatus_to_yesterday.sh
chmod +x data/set_logstatus_to_yesterday.sh
data/set_logstatus_to_yesterday.sh

# perform rotation operation
logrotate -v /etc/logrotate.d/test.conf

# check if rotated log was created
if ls /var/log/testlog.1 ; then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

# cleanup
rm -f /etc/logrotate.d/test.conf
rm -f /var/log/testlog*
rm -f data/set_logstatus_to_yesterday.sh
if [ -f ${STATUS_FILE}_bak ] ; then
    mv ${STATUS_FILE}_bak ${STATUS_FILE}
else
    rm ${STATUS_FILE}
fi
