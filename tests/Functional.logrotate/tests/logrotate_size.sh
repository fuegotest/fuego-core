#!/bin/sh
#  Verify that the rotate file is created in the specified directory
#  when the logfile is over the size parameter specified in the config.
#  option : -v

test="log rotation size"

if [ -z "${STATUS_FILE}" ] ; then
    STATUS_FILE=/var/lib/logrotate/status
fi

# setup - clear old logs, reset status, and set config
if [ -f /var/log/testlog* ]
then
    rm -f /var/log/testlog*
fi

if [ -f ${STATUS_FILE} ] ; then
    mv ${STATUS_FILE} ${STATUS_FILE}_bak
fi

cp data/test.conf.size /etc/logrotate.d/test.conf
chown root /etc/logrotate.d/test.conf

cp data/testlog100k /var/log/testlog

cp data/testlog.sh data/set_logstatus_to_yesterday.sh
chmod +x data/set_logstatus_to_yesterday.sh
data/set_logstatus_to_yesterday.sh

# perform rotation operation
logrotate -v /etc/logrotate.d/test.conf
if ls /var/log/testlog.1 ; then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

# cleanup
rm -f /etc/logrotate.d/test.conf
rm -f /var/log/testlog*
rm -f data/set_logstatus_to_yesterday.sh
if [ -f ${STATUS_FILE}_bak ] ; then
    mv ${STATUS_FILE}_bak ${STATUS_FILE}
else
    rm ${STATUS_FILE}
fi
