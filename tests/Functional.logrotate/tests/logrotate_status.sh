#!/bin/sh
# check if status file has entry for testlog when rotating a file
#
#  Turns on verbose mode.
#  option : -v

test="log status add"

if [ -z "${STATUS_FILE}" ] ; then
    STATUS_FILE=/var/lib/logrotate/status
fi

# setup - clear status, set config
if [ -f ${STATUS_FILE} ] ; then
    mv ${STATUS_FILE} ${STATUS_FILE}_bak
fi
cp data/test.conf /etc/logrotate.d/test.conf

# perform operation
logrotate -v /etc/logrotate.d/test.conf

# see if testlog is in the status file
if tail ${STATUS_FILE} | grep testlog ; then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

# cleanup
rm -f /etc/logrotate.d/test.conf
if [ -f ${STATUS_FILE}_bak ] ; then
    mv ${STATUS_FILE}_bak ${STATUS_FILE}
else
    rm ${STATUS_FILE}
fi
