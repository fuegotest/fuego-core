#!/bin/sh

#  In target, run comannd ltrace to display system calls.
#  option: -S

test="syscall"

rm log_for_fuego || true
ltrace -S ls > log_for_fuego 2>&1
if grep "SYS_getdents" log_for_fuego
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log_for_fuego
