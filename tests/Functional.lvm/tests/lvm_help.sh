#!/bin/sh

#  In target, display the help text.
#  option: none

test="help"

if lvm help
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
