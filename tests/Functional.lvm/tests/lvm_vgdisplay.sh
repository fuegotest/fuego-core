#!/bin/sh

#  In target, display attributes of volume groups.
#  option: none

test="vgdisplay"

if vgdisplay
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
