#!/bin/sh
export MODULE_NAME=$1

if [ -z "$MODULE_NAME" ] ; then
    echo "Aborting: missing required argument for MODULE_NAME"
    exit 1
fi

echo "Running tests with module: $MODULE_NAME"

for i in tests/*.sh; do
    sh $i
done
