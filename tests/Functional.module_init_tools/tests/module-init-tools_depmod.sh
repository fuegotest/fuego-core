#!/bin/sh

#  In target, run command depmod.
#  option: none

test="depmod"

test_krnl_rel=$(uname -r)

if [ -f /lib/modules/$test_krnl_rel/modules.dep ]
then
    mv /lib/modules/$test_krnl_rel/modules.dep /lib/modules/$test_krnl_rel/modules.dep_bak
fi

depmod

if ls -l /lib/modules/$test_krnl_rel/modules.dep
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ -f /lib/modules/$test_krnl_rel/modules.dep_bak ]
then
    mv /lib/modules/$test_krnl_rel/modules.dep_bak /lib/modules/$test_krnl_rel/modules.dep
else
    rm /lib/modules/$test_krnl_rel/modules.dep
fi
