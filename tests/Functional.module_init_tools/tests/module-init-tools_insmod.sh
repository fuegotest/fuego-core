#!/bin/sh

#  In target, run command insmod.
#  option: none

test="insmod"

tst_mod_file="ko file of module"

if modinfo $MODULE_NAME
then
    tst_mod_file=$(modinfo $MODULE_NAME | head -n 1 | cut -d' ' -f8)
else
    echo " -> module $MODULE_NAME does not exist."
    echo " -> $test: TEST-FAIL"
    exit
fi

mkdir test_dir
cp $tst_mod_file test_dir/

# If the module is already loaded, the insmod test goes pass.
if lsmod | grep $MODULE_NAME
then
    echo " -> $test: TEST-PASS"
    rm -fr test_dir
    exit 0
fi

if insmod test_dir/*.ko
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rmmod $tst_mod_file
rm -fr test_dir
