#!/bin/sh

#  In target, run command modprobe.
#  option: modprobe

test="modprobe"

tst_mod_file="ko file of module"
test_krnl_rel=$(uname -r)

if modinfo $MODULE_NAME
then
    tst_mod_file=$(modinfo $MODULE_NAME | head -n 1 | cut -d' ' -f8)
else
    echo " -> module $MODULE_NAME does not exist."
    echo " -> $test: TEST-FAIL"
    exit
fi

# '-D' means only print module dependencies and exit
if modprobe -D $MODULE_NAME | grep "insmod $tst_mod_file"
then
    echo " -> $test: modprobe -D succeeded."
else
    echo " -> $test: modprobe -D failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if cat /lib/modules/$test_krnl_rel/modules.dep | grep "$MODULE_NAME.ko"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
