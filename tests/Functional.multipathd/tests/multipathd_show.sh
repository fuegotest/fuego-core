#!/bin/sh

#  In the target start multipathd, and show the format wildcards used in interactive commands taking $format.

test="show"

service_status=$(get_service_status multipathd)

exec_service_on_target multipathd stop

if [ -f /etc/multipath.conf ]
then
    cp /etc/multipath.conf /etc/multipath.conf_bak
fi

if [ -f /etc/multipath.conf.example ]
then
    cp /etc/multipath.conf.example /etc/multipath.conf
fi

restore_target() {
    if [ -f /etc/multipath.conf_bak ]
    then
        mv /etc/multipath.conf_bak /etc/multipath.conf
    else
        rm -f /etc/multipath.conf
    fi
}

if exec_service_on_target multipathd start
then
    echo " -> start of multipathd succeeded."
else
    echo " -> start of multipathd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

if multipathd show wildcards | grep "multipath format wildcards:"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

restore_target
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target multipathd start
fi
