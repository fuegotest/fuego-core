#!/bin/sh

#  In the target start multipathd, and check the messages of /var/log/syslog.
#  check the keyword "multipathd".

test="syslog"

logger_service=$(detect_logger_service)

service_status=$(get_service_status multipathd)

exec_service_on_target multipathd stop
exec_service_on_target $logger_service stop

if [ -f /etc/multipath.conf ]
then
    cp /etc/multipath.conf /etc/multipath.conf_bak
fi

if [ -f /etc/multipath.conf.example ]
then
    cp /etc/multipath.conf.example /etc/multipath.conf
fi

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

restore_target() {
    if [ -f /etc/multipath.conf_bak ]
    then
        mv /etc/multipath.conf_bak /etc/multipath.conf
    else
        rm -f /etc/multipath.conf
    fi

    if [ -f /var/log/syslog_bak ]
    then
        mv /var/log/syslog_bak /var/log/syslog
    fi
}

exec_service_on_target $logger_service restart

if exec_service_on_target multipathd start
then
    echo " -> start of multipathd succeeded."
else
    echo " -> start of multipathd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 10

if cat /var/log/syslog | grep "multipath"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target multipathd stop
restore_target
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target multipathd start
fi
