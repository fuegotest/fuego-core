#!/bin/sh

#  In the target start snmpd.
# Check the log of snmpd.

test="snmpd_syslog"

logger_service=$(detect_logger_service)

snmpd_status=$(get_service_status snmpd)
snmpd_logfile=$(get_service_logfile)

exec_service_on_target snmpd stop
exec_service_on_target $logger_service stop

if [ -f $snmpd_logfile ]
then
    mv $snmpd_logfile $snmpd_logfile"_bak"
fi

restore_target() {
    if [ -f $snmpd_logfile"_bak" ]
    then
        mv $snmpd_logfile"_bak" $snmpd_logfile
    fi
}

exec_service_on_target $logger_service restart

sleep 2

if exec_service_on_target snmpd start
then
    echo " -> start of snmpd succeeded."
else
    echo " -> start of snmpd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 3

if cat $snmpd_logfile | grep "snmpd"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$snmpd_status" = "inactive" ]
then
    exec_service_on_target snmpd stop
fi
restore_target
