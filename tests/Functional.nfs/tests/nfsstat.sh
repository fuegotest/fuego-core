#!/bin/sh

#  In the target run the command nfsstat, and display statistics kept
#  about NFS client and server activity.
#  option : -r, -c, -s

test="nfsstat"

if nfsstat -c | grep "Client"
then
    echo " -> Print only client-side statistics succeeded."
else
    echo " -> Print only client-side statistics failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsstat -r | grep "rpc"
then
    echo " -> Print only RPC statistics succeeded."
else
    echo " -> Print only RPC statistics failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsstat -s | grep "Server"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> Print only server-side statistics failed."
    echo " -> $test: TEST-FAIL"
fi
