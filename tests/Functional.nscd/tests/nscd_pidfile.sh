#!/bin/sh

#  In the target start nscd, and check if the /var/run/nscd.pid is exist
#  check the keyword "nscd".

test="pidfile"

nscd_status=$(get_service_status nscd)

exec_service_on_target nscd stop

if [ -f /run/nscd/nscd.pid ]
then
    rm -f /run/nscd/nscd.pid
fi

if exec_service_on_target nscd start
then
    echo " -> start of nscd succeeded."
else
    echo " -> start of nscd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 10

if test -f /run/nscd/nscd.pid
then
    echo " -> get the pidfile of nscd."
else
    echo " -> can't get the pidfile of nscd."
    echo " -> $test: TEST-FAIL"
    if [ "$nscd_status" = "inactive" ]
    then
        exec_service_on_target nscd stop
    fi
    exit
fi

exec_service_on_target nscd stop

if test ! -f /run/nscd/nscd.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$nscd_status" = "active" -o "$nscd_status" = "unknown" ]
then
    exec_service_on_target nscd start
fi
