#!/bin/sh

#  In the target start nscd, and check the messages of /var/log/syslog.
#  check the keyword "nscd".

test="syslog"

logger_service=$(detect_logger_service)

nscd_status=$(get_service_status nscd)

exec_service_on_target nscd stop
exec_service_on_target $logger_service stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

restore_target(){
    if [ -f /var/log/syslog_bak ]
    then
        mv /var/log/syslog_bak /var/log/syslog
    fi
}

exec_service_on_target $logger_service restart

if exec_service_on_target nscd start
then
    echo " -> start of nscd succeeded."
else
    echo " -> start of nscd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 10

if cat /var/log/syslog | grep "starting up"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$nscd_status" = "inactive" ]
then
    exec_service_on_target nscd stop
fi
restore_target
