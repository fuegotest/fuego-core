#!/bin/sh

#  In the target start openct, and confirm the status.

test="status"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status openct)

exec_service_on_target openct stop

if exec_service_on_target openct start
then
    echo " -> start of openct succeeded."
else
    echo " -> start of openct failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if [ "$init_manager" = "systemd" ]
then
    if systemctl is-active openct
    then
        echo " -> $test: TEST-PASS"
    else
        echo " -> $test: TEST-FAIL"
    fi
else
    if service openct status | grep "Waiting for reader attach/detach events"
    then
        echo " -> $test: TEST-PASS"
    else
        echo " -> $test: TEST-FAIL"
    fi
fi

if [ "$service_status" = "inactive" ]
then
    exec_service_on_target openct stop
fi
