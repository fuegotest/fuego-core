#!/bin/sh
. ./fuego_board_function_lib.sh
set_init_manager

. data/testenv.sh

for i in tests/*.sh; do
    if [ "$i" = "tests/openvpn_genkey.sh" ]; then
        source $i
    else
        source $i client
        source $i server
    fi
done
