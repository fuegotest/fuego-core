#!/bin/sh

#  In the target start ospf6d and zebra, then confirm the process condition by command ps.
#  check the keyword "quagga/ospf6d".

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop

#Backup the config file
mv /etc/quagga/ospf6d.conf /etc/quagga/ospf6d.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/ospf6d.conf /etc/quagga/ospf6d.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target ospf6d start
then
    echo " -> start of ospf6d succeeded."
else
    echo " -> start of ospf6d failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep "quagga/[o]spf6d"
then
    echo " -> get the pid of ospf6d."
else
    echo " -> can't get the pid of ospf6d."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop

if ps -N a | grep "quagga/[o]spf6d"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

#Restore the config file
mv /etc/quagga/ospf6d.conf.bck /etc/quagga/ospf6d.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf
