#!/bin/sh

# In target, run script xml-simple-test.
# To make sure that the string "<test>XML::Simple.test</test>" is in output.

test="xml-simple-test01"

if perl data/xml-simple-test | grep ".*<test>XML::Simple.test</test>.*"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

rm -f data/xml-simple-test
