#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys, collections
import common as plib
import re

measurements = {}
measurements = collections.OrderedDict()

with open(plib.TEST_LOG) as f:
    print("Reading current values from " + plib.TEST_LOG +"\n")
    lines = f.readlines()

for line in lines:
    line = line.strip()
    if line.startswith('BEGIN:'):
        # BEGIN: /usr/lib/gzip/ptest <-- we want gzip
        test_set = line.split(':')[1].strip().split('/')[-2]
        continue
    elif line.startswith('PASS:') or line.startswith('XPASS:'):
        # PASS: zlib shared <-- we want zlib_shared
        test_case = line.split(':')[1].strip().replace(' ', '_')
        result = 'PASS'
    elif line.startswith('SKIP:'):
        test_case = line.split(':')[1].strip().replace(' ', '_')
        result = 'SKIP'
    elif line.startswith('FAIL:') or line.startswith('XFAIL:') or line.startswith('ERROR:'):
        test_case = line.split(':')[1].strip().replace(' ', '_')
        result = 'FAIL'
    elif line.startswith('END:'):
        continue
    else:
        # TODO: split output per test here
        continue
    measurements[test_set + '.' + test_case] = result

sys.exit(plib.process(measurements))
