# assume we start in top directory of kernel source tree

# old code for pipeline build
#cd ${JENKINS_HOME}/jobs/pipeline/BUILD_${JOB_NAME}_${BUILD_NUMBER}/arch/arm/configs/

pushd arch/arm/configs

# Checking the config of ipv6 and PRIO_SCHED modules in bcm2709_defconfig

if grep -i CONFIG_IPV6=y "bcm2709_defconfig"; then
    echo "Congiguration IPV6 is already set "
else
    if grep -i CONFIG_IPV6=m "bcm2709_defconfig"; then
        sed -i -- 's/CONFIG_IPV6=m/CONFIG_IPV6=y/g' bcm2709_defconfig
        echo "Configuration IPV6 changed in bcm2709_defconfig"
    else
        echo "CONFIG_IPV6=y" >> bcm2709_defconfig
        echo "Configuration IPV6 added in bcm2709_defconfig"
    fi
fi

if grep -i CONFIG_RT_PRIO_SCHED=y "bcm2709_defconfig"; then
    echo "Congiguration RT_PRIO_SCHED is already set"
else
    if grep -i CONFIG_RT_PRIO_SCHED=m "bcm2709_defconfig"; then
        sed -i -- 's/CONFIG_RT_PRIO_SCHED=m/CONFIG_RT_PRIO_SCHED=y/g' bcm2709_defconfig
        echo "Configuration RT_PRIO_SCHED changed in bcm2709_defconfig"
    else
        echo "CONFIG_RT_PRIO_SCHED=y" >> bcm2709_defconfig
        echo "Configuration RT_PRIO_SCHED added in bcm2709_defconfig"
    fi
fi
popd

# create the .config, at the root of the source tree
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2709_defconfig

# add extra information to the kernel version
# short commmit id, and build number
GIT_AND_BUILD_VER="$(git rev-parse --short HEAD)-${BUILD_NUMBER}"

# Append CONFIG_LOCALVERSION with Build_Number in .config file
if grep -i 'CONFIG_LOCALVERSION="-v7"' ".config"; then
    sed -i -- 's/CONFIG_LOCALVERSION="-v7"/CONFIG_LOCALVERSION="'-v7-${GIT_AND_BUILD_VER}'"/g' .config
    echo "configuration LOCALVERSION changed in .config file to append ${GIT_AND_BUILD_VER}"
else
    echo "config LOCALVERSION=-v7 not found - failed"
fi

make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs

# make modules.tgz file
rm -rf modules.tgz || true
rm -rf modules-staging || true
mkdir -p modules-staging

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=modules-staging modules_install
pushd modules-staging
tar -czf modules.tgz *
popd
mv modules-staging/modules.tgz .
