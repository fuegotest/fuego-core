NEED_ROOT=1

function test_pre_check {
    assert_has_program samba
    assert_has_program expect
    assert_has_program smbclient
    assert_has_program sed
}

function test_deploy {
    put $TEST_HOME/samba_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export test_target=$IPADDR;\
    export test_host=$SRV_IP;\
    ./samba_test.sh"

    service_file=/tmp/test_for_fuego
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sh tests/test_samba06.sh $SRV_IP $IPADDR $service_file"

    log_this "cd $TEST_HOME; sh tests/test_samba06_host.sh $IPADDR"

    cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sh tests/restore_target.sh $service_file"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
