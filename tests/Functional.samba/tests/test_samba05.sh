#!/bin/sh

#  In the target start samba, set the server to access
#  the target having password from the server,
#  and make sure the server can access the target.

test="samba05"

. ./fuego_board_function_lib.sh

set_init_manager

useradd test_for_fuego

expect <<-EOF
spawn smbpasswd -a test_for_fuego
expect "SMB password:"
send "goodluck\r"
expect "Retype new SMB password:"
send "goodluck\r"
expect "Added user root."
EOF

smb_status=$(get_service_status smb)
iptables_status=$(get_service_status iptables)

exec_service_on_target smb stop
exec_service_on_target iptables stop

mkdir -p /home/test/samba

cp data/test1 /home/test/samba/

chown nobody:nobody -R /home/test/samba

if [ -f /etc/samba/smb.conf ]
then
    mv /etc/samba/smb.conf /etc/samba/smb.conf_bak
fi

restore_target(){
    if [ -f /etc/samba/smb.conf_bak ]
    then
        mv /etc/samba/smb.conf_bak /etc/samba/smb.conf
    fi
    smbpasswd -x test_for_fuego
    rm -fr /home/test_for_fuego
    userdel test_for_fuego
    if [ "$iptables_status" = "active" -o "$iptables_status" = "unknown" ]
    then
        exec_service_on_target iptables start
    fi
    if [ "$smb_status" = "inactive" ]
    then
        exec_service_on_target smb stop
    fi
}

sed -i 's/test_target/'"$test_target"'/g' data/smb05.conf
cp data/smb05.conf /etc/samba/smb.conf

if exec_service_on_target smb start
then
    echo " -> start of smb succeeded."
else
    echo " -> start of smb failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 5

expect <<-EOF
spawn sh
expect ".*"
send "smbclient //$test_target/test --user=test_for_fuego\r"
expect {
 -re ".*Enter.*password:.*" {
           send "goodluck\r"
          }
 default { send_user "Can not access the board.\n"}  }
expect {
 -re ".*Password:.*" {
           send "goodluck\r"
          }
 default { send_user "Password is wrong.\n"}  }
expect {
 -re ".*smb:.*" {
           send "ls\r"
          }
 default { send_user "Can not log into the board.\n"}  }
expect {
 -re ".*test1.*" {
           send_user " -> $test: TEST-PASS\n"
          }
 default { send_user " -> $test: TEST-FAIL\n"}  }
send "exit\n"
expect eof
EOF

restore_target
