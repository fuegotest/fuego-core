NEED_ROOT=1
NO_BOARD_TESTLOG=1

function test_run {
    log_this "echo TAP version 13"
    log_this "echo 1..1"

    # FEATURE IDEA: add code to change the time to something way off
    # (and validate that it stuck), before doing this operation
    # This would validate that the time is settable on the machine.

    cmd "date $(date +%m%d%H%M%Y.%S)"

    # execute the following date commands as close together as possible

    # get DUT seconds first, because cmd execution time might be long
    # but the execution of 'date' and the value return should be pretty quick
    DUT_SECONDS=$(cmd "date +%s")
    HOST_SECONDS=$(date +%s)

    log_this "echo HOST_SECONDS=$HOST_SECONDS DUT_SECONDS=$DUT_SECONDS"

    DELTA=$(( HOST_SECONDS - DUT_SECONDS ))

    # remove leading '-' if any (to make it absolute value)
    # allow up to 2-second DELTA for the overhead of 'cmd'
    if [ ${DELTA#-} -gt 2 ] ; then
        log_this "echo not ok 1 Time is synchronized with host"
    else
        log_this "echo ok 1 Time is synchronized with host"
    fi
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
