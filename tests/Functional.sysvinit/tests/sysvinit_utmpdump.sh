#!/bin/sh

#  In target, run command utmpdump.

test="utmpdump"

if utmpdump /var/log/wtmp
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
