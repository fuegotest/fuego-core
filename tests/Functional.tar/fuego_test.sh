function test_pre_check {
    is_on_target_path tar PROGRAM_TAR
    assert_define PROGRAM_TAR "Missing 'tar' program on target board"
}

function test_deploy {
    put $TEST_HOME/tar_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sh -v tar_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
