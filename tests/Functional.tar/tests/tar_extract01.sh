#!/bin/sh

#  In target, run command tar.
#  option: xf

test="extract01"

if tar xf data/test.tar
then
    echo " -> tar xf succeeded."
else
    echo " -> tar xf failed."
fi

if test -f test/data/test_a.txt
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test
