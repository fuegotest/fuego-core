#!/bin/sh

#  In target, run command tar.
#  option: tf

test="list01"

if tar tf data/test.tar | grep "test_a.txt"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
