#!/bin/sh

#  In target, run command tcsd.
#  option: -h

test="help"

tcsd --help > log 2>&1
if cat log | grep "usage"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log
