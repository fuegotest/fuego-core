function test_pre_check {
    assert_has_program vconfig
    assert_has_program tcpdump
    assert_has_program ping
}

function test_deploy {
    put $TEST_HOME/vconfig_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export vcon_ifeth=$IFETH;\
    export vcon_ip1=$VCON_IP1;\
    export vcon_ip2=$VCON_IP2;\
    ./vconfig_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
