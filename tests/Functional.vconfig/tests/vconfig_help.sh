#!/bin/sh

#  In target, run command vconfig.
#  option: none

test="help"

if vconfig | grep Usage
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
