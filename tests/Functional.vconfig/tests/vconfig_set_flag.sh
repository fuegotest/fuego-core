#!/bin/sh

#  In target, run command vconfig.
#  option: set_flag

test="set_flag"

vconfig add $vcon_ifeth 4

if vconfig set_flag $vcon_ifeth.4 1 | grep "Set flag on device"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
vconfig rem $vcon_ifeth.4
