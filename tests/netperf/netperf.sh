function test_build {
    echo "ac_cv_func_setpgrp_void=yes" > config.cache
    # get updated config.sub and config.guess files, so configure
    # doesn't reject new toolchains
    cp /usr/share/misc/config.{sub,guess} .
    ./configure --build=`./config.guess` --host=$HOST CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" --config-cache
    make
}

